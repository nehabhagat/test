/*_____________________ OTA upgrade through HTTP(s) _________________________*/

#include "qcom_common.h"
#include "qcom_uart.h"
#include "qcom_internal.h"
#include "qcom/qcom_cli.h"
#include "qcom_ssl.h"
#include "qcom_utils.h"
#include "qcom/socket_api.h"
#include "qcom/select_api.h"
#include "qcom/qcom_wps.h"
#include "qcom/qcom_gpio.h"
#include "qcom/qcom_adc_api.h"
#include "qcom/qcom_network.h"
#include "qcom/qcom_nvram.h"

//#include "aj_target_nvram.h"
#include "qcom/qcom_dset.h"
#include "dsetid.h"
#include "dset_api.h"

A_INT32 flash_erase(A_UINT32 addr, A_UINT32 size);
int parse_ipv4_ad(unsigned int * ip_address,   /* pointer to IP address returned */
        unsigned *  sbits,      /* default subnet bit number */
        char *   stringin);
void AT_PTF(char* format,...);
#define RDLR_OK                             0
#define RDLR_OK_STR                         "\"OK\""

#define QBC_SAVE_FLAG                       0x55AA55AA
#define QBC_OTA_FLAG                        0xB51B51B5
#define QBC_CHKSUM_ADDR                     0x000c0A00

#define OTA_HTTPS_MAX_FILE_LEN          	(256)
#define OTA_HTTPS_MAX_SERVER_LEN        	(64)
#define OTA_HTTPS_REQ_MAX           		(600)           /* 192(field of header) + length of file name + length of server name */
#define OTA_HTTPS_RCV_MAX           		(2 * 1024)
#define OTA_HTTPS_TIMEOUT               	(10000)
#define OTA_HTTPS_PORT                  	(443)
#define OTA_HTTPS_ALT_PORT              	(46030)

#define SSL_OTA_INBUF_SIZE           		20000
#define SSL_OUTBUF_SIZE              		3500

#define OTA_CHKSUM 0
#if OTA_CHKSUM
#define ENABLE_OTA_CHKSUM 1
#else
#define ENABLE_OTA_CHKSUM 0
#endif

typedef PREPACK struct _QBC_FLASH_CHKSUM
{
    int  flag;          // CHKSUM_PARAM_FLAG
    unsigned char fw_chksum[32];

}qbc_flash_chksum;

qbc_flash_chksum qbc_chksum_param;

typedef struct _QCOM_OTA_HTTPS_REQ {
        char    *server;
        A_INT32 ip_addr;
        A_UINT16 port;
        char    *file;
        char    *message;
        A_INT32 size;
        struct  sockaddr_in ip;
} QCOM_OTA_HTTPS_REQ;

typedef struct _QCOM_OTA_HTTPS_RES {
        char    *buf;
        A_INT32 buf_length;

        A_INT32 response_code;
        A_INT32 content_length;
        A_INT32 header_length;
        A_BOOL  parsed_ota_header;

        A_INT32 read_length;
        A_INT32 partition_size;
} QCOM_OTA_HTTPS_RES;


typedef struct _QCOM_OTA_HTTP {
        A_INT32                 sock_fd;
        A_INT16                 port;

        QCOM_OTA_HTTPS_REQ      *req;
        QCOM_OTA_HTTPS_RES      *res;

        SSL_CTX                 *ssl_ctx;
        SSL                     *hssl;
} QCOM_OTA_HTTPS;

typedef A_INT32 (* OTA_HTTPS_RESP_CB)(
                char* buf,
                A_INT32 len,
                A_INT32 tot,
                A_UINT32 *img_offset
                );
/*******************************************************************************************/

A_UINT8  bIsChecksumDone    = 0;

void qbc_save_chksum (qbc_flash_chksum *param) {

    param->flag = QBC_OTA_FLAG;
    flash_erase(QBC_CHKSUM_ADDR, sizeof(qbc_flash_chksum));
    qcom_nvram_write (QBC_CHKSUM_ADDR, (unsigned char *)param, sizeof(qbc_flash_chksum));

}

int qbc_load_chksum (qbc_flash_chksum *param) {

    qcom_nvram_read (QBC_CHKSUM_ADDR, (unsigned char *)param, sizeof(qbc_flash_chksum));
    if (param->flag == QBC_OTA_FLAG) {
        return 1;
    } else {
        return -1;
    }
}

A_INT32 _qcom_ota_https_program(char *buf, A_INT32 len, A_INT32 tot, A_UINT32 *img_offset)
{
    A_UINT32 offset = 0, ret_size = 0, write_size = 0;
    A_INT32 ret = QCOM_OTA_OK;

    if ((ret = qcom_ota_partition_erase_sectors(*img_offset + len)) != QCOM_OTA_OK) {
//        AT_PTF("OTA Partition Erase Fail\n");
        return ret;
    }

    while (len > 0) {
        if (len > MAX_OTA_AREA_READ_SIZE) {
            write_size = MAX_OTA_AREA_READ_SIZE;
        } else {
            write_size = len;
        }

        len -= write_size;

        if ((ret = qcom_ota_partition_write_data(*img_offset,
                               (A_UINT8 *)&buf[offset], write_size, &ret_size)) != QCOM_OTA_OK) {
//            AT_PTF("OTA write error(%d), write_size(%d), ret_size(%d)\n", ret, write_size, ret_size);
            return ret;
        }

//        AT_PTF(".");

        offset += ret_size;
        *img_offset += ret_size;
    }

    return QCOM_OTA_OK;
}

A_INT32 _qcom_ota_https_connect(QCOM_OTA_HTTPS *ota_https, A_UINT32 timeout)
{
    struct timeval tv;
    q_fd_set wset;
    A_INT32 ret;

    FD_ZERO(&wset);
    tv.tv_sec = (timeout/1000);
    tv.tv_usec = 0;

    FD_SET(ota_https->sock_fd, &wset);
    if ((ret = qcom_connect(ota_https->sock_fd, (struct sockaddr*)&ota_https->req->ip, sizeof(ota_https->req->ip))) < 0) {
//        printf("Connection failed.\n");
		AT_PTF("ERROR-9.3");
        return ret;
    }

    if ((ret = qcom_select(ota_https->sock_fd + 1, 0, &wset, 0, &tv)) <= 0) {
        if (ret == 0) return (-QCOM_OTA_ERR_SERVER_RSP_TIMEOUT);
        else return (ret);
    }

    if (ota_https->req->port == OTA_HTTPS_PORT || ota_https->req->port == OTA_HTTPS_ALT_PORT) {
        if ((ret = qcom_SSL_connect(ota_https->hssl)) < 0) {
//            A_PRINTF("ssl connect error(%d)\n", ret);
            return ret;
        }
    }

    return 0;
}

void _qcom_ota_https_close(QCOM_OTA_HTTPS *ota_https)
{
    QCOM_OTA_HTTPS_REQ *req;
    QCOM_OTA_HTTPS_RES *res;

    if (!ota_https) return;

    req = ota_https->req;
    res = ota_https->res;

    if (req) {
        if (req->file) qcom_mem_free(req->file);
        if (req->server) qcom_mem_free(req->server);
        if (req->message) qcom_mem_free(req->message);

        req->file = 0;
        req->server = 0;
        req->message = 0;

        qcom_mem_free(req);
        ota_https->req = 0;
    }

    if (res) {
        if (res->buf) qcom_mem_free(res->buf);
        res->buf = 0;

        qcom_mem_free(res);
        ota_https->res = 0;
    }

    if (ota_https->ssl_ctx) {
        qcom_SSL_ctx_free(ota_https->ssl_ctx);
        ota_https->ssl_ctx = 0;
    }
    if (ota_https->hssl){
        qcom_SSL_shutdown(ota_https->hssl);
        ota_https->hssl = 0;
    }

    if (ota_https->sock_fd > 0) {
        qcom_socket_close(ota_https->sock_fd);
    }

    qcom_mem_free(ota_https);

    return;
}

A_INT32 _qcom_ota_https_parse_header_content_length(char *buf)
{
#define _MAX_CONTENT_DIGIT      (20)
    char *str;
    char temp[_MAX_CONTENT_DIGIT];
    A_INT32 i = 0;

    str = A_STRSTR(buf, "Content-Length: ");
    if (str) {
        str += 16;

        while (*str) {
            if (i >= _MAX_CONTENT_DIGIT)
                return (-1);

            if (*str == '\r' && *(str+1) == '\n') {
                break;
            } else {
                temp[i] = *str;
            }

            i++; str++;
        }

        temp[i] = 0;
        return atoi(temp);
    }

    return (-1);
}

A_INT32 _qcom_ota_https_parse_header_response_code(char *buf)
{
    A_INT32 response_code;
    A_INT32 ver1, ver2, i;

    i = sscanf(buf, "HTTP/%d.%d %d ", &ver1, &ver2, &response_code);
    if (i != 3) return (-1);

    return response_code;
}

A_INT32 _qcom_ota_https_check_header(QCOM_OTA_HTTPS *ota_https)
{
    QCOM_OTA_HTTPS_RES *res = ota_https->res;
    char *hend;
    A_INT32 len;

    if (res->response_code > 0) return res->response_code;
    if (!res->buf_length) return 0;

    hend = A_STRSTR(res->buf, "\r\n\r\n");
    if (!hend) return 0;

    *hend = 0;

    res->response_code = _qcom_ota_https_parse_header_response_code(res->buf);
    res->content_length = _qcom_ota_https_parse_header_content_length(res->buf);

    if (res->response_code >= 300) return -(res->response_code);

    len = res->buf_length - (A_INT32)((hend + 4) - res->buf);
    res->header_length = res->buf_length - len;

    A_MEMMOVE(res->buf, hend + 4, len);
    res->buf_length = len;

    return res->response_code;
}

A_INT32 _qcom_ota_https_read_packet(QCOM_OTA_HTTPS *ota_https, A_UINT32 timeout)
{
    QCOM_OTA_HTTPS_RES *res = ota_https->res;
    struct timeval tv;
    q_fd_set rset;
    A_INT32 len, ret;

    FD_ZERO(&rset);
    tv.tv_sec = (timeout/1000);
    tv.tv_usec = 0;

    FD_SET(ota_https->sock_fd, &rset);
    if ((ret = qcom_select(ota_https->sock_fd + 1, &rset, 0, 0, &tv)) <= 0) {
        if (ret == 0) return 0;
        else return (ret);
    }

    if (ota_https->req->port == OTA_HTTPS_PORT || ota_https->req->port == OTA_HTTPS_ALT_PORT) {
        len = qcom_SSL_read(ota_https->hssl, &res->buf[0], OTA_HTTPS_RCV_MAX);
        if (len > 0) {
            res->buf_length = len;
        } else {
//            AT_PTF("SSL read error(%d)\n", len);
            return (-1);
        }
    } else {
        len = qcom_recv(ota_https->sock_fd, &res->buf[0], OTA_HTTPS_RCV_MAX, 0);
        if (len > 0) {
            res->buf_length = len;
        } else {
//            AT_PTF("qcom_recv error(%d)\n", len);
            return (-1);
        }
    }
    return len;
}

A_INT32 _qcom_ota_https_query(QCOM_OTA_HTTPS *ota_https)
{
    QCOM_OTA_HTTPS_REQ *req = ota_https->req;
    A_INT32 ret = QCOM_OTA_OK;
    char *cp = req->message;

    cp += sprintf(cp, "GET /%s HTTP/1.1\r\n", req->file);
    cp += sprintf(cp, "Host: %s\r\n", req->server);
    cp += sprintf(cp, "Accept: text/html, */*\r\n");
    cp += sprintf(cp, "User-Agent: IOE Client\r\n");
    cp += sprintf(cp, "Connection: keep-alive\r\n");
    cp += sprintf(cp, "Cache-control: no-cache\r\n");
    cp += sprintf(cp, "\r\n");

    req->size = A_STRLEN(req->message);
//    AT_PTF("%s\nHTTP(S) query message size: %d\n\n", req->message, req->size);

    if (req->port == OTA_HTTPS_PORT || req->port == OTA_HTTPS_ALT_PORT) {
        if ((ret = qcom_SSL_write(ota_https->hssl, req->message, req->size)) < 0) {
//            AT_PTF("ssl write error(%d)\n", ret);
        }
    } else {
    	if ((ret = qcom_send(ota_https->sock_fd, req->message, req->size, 0)) < 0) {
//            AT_PTF("qcom_send error(%d)\n", ret);
		}
    }

    return ret;
}

static A_INT32 _qcom_ota_https_request(
                QCOM_OTA_HTTPS          *ota_https,
                OTA_HTTPS_RESP_CB       func,
                A_UINT32                timeout,
                A_UINT32 *size)
{
    QCOM_OTA_HTTPS_RES *res;
    A_INT32 ret = QCOM_OTA_OK , ret_t = -1;
    A_UINT32 offset = 0, img_offset = 0;

#ifdef ENABLE_OTA_CHKSUM
    qbc_flash_chksum *param = &qbc_chksum_param;
	memset(param, 0, sizeof(*param));
	ret_t = qbc_load_chksum (param);
#endif

    res = ota_https->res;
    res->read_length = 0;
    *size = 0;

    qcom_socket_set_non_blocking(ota_https->sock_fd, 1);
    if ((ret = _qcom_ota_https_connect(ota_https, timeout)) < 0)
            return ret;

    if ((ret = _qcom_ota_https_query(ota_https)) < 0) {
            return ret;
    }

    do {
        ret = _qcom_ota_https_read_packet(ota_https, timeout);
        if (ret < 0) {
//	    	AT_PTF("https read error\n");
            return ret;
        }

        if (!res->response_code) {
            ret = _qcom_ota_https_check_header(ota_https);
            if (ret == 0) continue; /* not finished header receive */
            else if (ret < 0) return ret; /* error */
//            AT_PTF("response: %d, content length: %d, header length: %d\n",
//                            res->response_code, res->content_length, res->header_length);

            if (res->content_length > res->partition_size || res->content_length < 0) {
                    ret = QCOM_OTA_ERR_IMAGE_DOWNLOAD_FAIL;
                    return ret;
            }

            if (!res->buf_length) {
                continue; // Wait for next packet
            }
        }

        res->read_length += res->buf_length;
        if (!res->parsed_ota_header) {
            if ((ret = qcom_ota_parse_image_hdr((A_UINT8 *)res->buf, &offset)) != 0) {
//                    AT_PTF("OTA parse error(%d)\n", ret);
                    return ret;
            }

            res->buf_length -= offset;
//            AT_PTF("OTA parse offset: %d, buf_length: %d\n", offset, res->buf_length);
            res->parsed_ota_header = TRUE;
        } else {
            offset = 0;
        }

        if (!func)
            continue;

#if ENABLE_OTA_CHKSUM
	    if (!bIsChecksumDone) {

	    	memset (digest, 0, 32);
    		qca_md5_calc (digest, (unsigned char *)res->buf, res->buf_length);

    		if ((ret_t == 1) && (0 == memcmp (param->fw_chksum, digest, 32))) {
//				AT_PTF("its same\n");
    			bIsChecksumDone   = 0xFF;
				break;
    		} else {
//				AT_PTF("its not same\n");
    			bIsChecksumDone   = 1;
    		}
		}
#endif
        if (res->buf_length) {
            if ((ret = func(&res->buf[offset], res->buf_length, res->read_length, &img_offset)) < 0) {
//                AT_PTF("OTA write error\n");
                return ret;
            }
        }


        res->buf_length = 0;
        if (res->read_length >= res->content_length) {
//            AT_PTF("read length: %d, img_offset: %d\n", res->read_length, img_offset);
            break;
		}
	} while (1);

#if ENABLE_OTA_CHKSUM
	if (bIsChecksumDone == 1) {
		memcpy (param->fw_chksum, digest, 32);
		qbc_save_chksum (param);
	}
#endif

	*size = res->read_length;
    return QCOM_OTA_OK;
}

QCOM_OTA_HTTPS *_qcom_ota_https_open(char *server, A_INT32 ip_addr, A_INT32 port, char *file, A_INT32 param)
{
    QCOM_OTA_HTTPS *ota_https = 0;
    struct sockaddr_in loc;
    A_UINT32 file_len, server_len;

    if (!port) port = 80;

    ota_https = (QCOM_OTA_HTTPS *)qcom_mem_alloc(sizeof(QCOM_OTA_HTTPS));
    if (!ota_https) {
		AT_PTF("ERROR-9.2");
//            AT_PTF("Memory allocate failed\n");
		goto _alloc_error;
    }
    A_MEMZERO(ota_https, sizeof(QCOM_OTA_HTTPS));

    ota_https->req = (QCOM_OTA_HTTPS_REQ *)qcom_mem_alloc(sizeof(QCOM_OTA_HTTPS_REQ));
    if (!ota_https->req) {
		AT_PTF("ERROR-9.2");
//            AT_PTF("Memory allocate failed\n");
		goto _alloc_error;
    }
    A_MEMZERO(ota_https->req, sizeof(QCOM_OTA_HTTPS_REQ));

    ota_https->req->ip_addr                 = ip_addr;
    ota_https->req->port                    = port;
    ota_https->req->message                 = (char *)qcom_mem_alloc(OTA_HTTPS_REQ_MAX);
    if (!ota_https->req->message) {
		AT_PTF("ERROR-9.2");
//            AT_PTF("Memory allocate failed\n");
		goto _alloc_error;
    }
    ota_https->req->size                    = OTA_HTTPS_REQ_MAX;

    /* TODO, check strdup & mem free routine, address mis-aligned exception happens when mem_free invoked
    ota_https->req->file                    = strdup(file);
    ota_https->req->server                  = strdup(server);
    */
    if ((file_len = A_STRLEN(file) + 1) > OTA_HTTPS_MAX_FILE_LEN) {
		AT_PTF("ERROR-9.3");
		goto _alloc_error;
    }

    ota_https->req->file                    = qcom_mem_alloc(file_len);
    if (!ota_https->req->file) {
		AT_PTF("ERROR-9.2");
//            AT_PTF("Memory allocate failed\n");
		goto _alloc_error;
    }

    if ((server_len = A_STRLEN(server) + 1) > OTA_HTTPS_MAX_SERVER_LEN) {
		goto _alloc_error;
    }

    ota_https->req->server                  = qcom_mem_alloc(server_len);
    if (!ota_https->req->server) {
		AT_PTF("ERROR-9.2");
//            AT_PTF("Memory allocate failed\n");	
		goto _alloc_error;
    }

    A_STRCPY(ota_https->req->file, file);
    A_STRCPY(ota_https->req->server, server);

    memset((char*)&ota_https->req->ip, 0, sizeof(struct sockaddr_in));
    ota_https->req->ip.sin_family           = AF_INET;
    ota_https->req->ip.sin_addr.s_addr      = htonl(ip_addr);
    ota_https->req->ip.sin_port             = htons(port);
    ota_https->sock_fd                      = qcom_socket(AF_INET, SOCK_STREAM, 0);
    if (ota_https->sock_fd < 0) {
		AT_PTF("ERROR-9.3");
		goto _socket_error;
    }

    memset((char*)&loc, 0, sizeof(struct sockaddr_in));
    loc.sin_family          = AF_INET;
    loc.sin_addr.s_addr     = INADDR_ANY;
    loc.sin_port            = htons((A_INT16)0);

    if (qcom_bind(ota_https->sock_fd, (struct sockaddr*)&loc, sizeof(struct sockaddr_in)) < 0) {
		goto _socket_error;
    }

    ota_https->port = loc.sin_port;
    ota_https->res = (QCOM_OTA_HTTPS_RES *)qcom_mem_alloc(sizeof(QCOM_OTA_HTTPS_RES));
    if (!ota_https->res) {
		AT_PTF("ERROR-9.2");
//            AT_PTF("Memory allocate failed\n");
		goto _alloc_error;
    }
    A_MEMZERO(ota_https->res, sizeof(QCOM_OTA_HTTPS_RES));

    ota_https->res->buf = (char *)qcom_mem_alloc(OTA_HTTPS_RCV_MAX);
    if (!ota_https->res->buf) {
		AT_PTF("ERROR-9.2");
//            AT_PTF("Memory allocate failed\n");
            goto _alloc_error;
    }

    if (port == OTA_HTTPS_PORT || port == OTA_HTTPS_ALT_PORT) {
        A_INT32 reuse = 1;
        A_INT32 yes = 1;
        struct linger lin;

        lin.l_onoff = FALSE;
        lin.l_linger = 5;

        qcom_socket_set_non_blocking(ota_https->sock_fd, 1);
        qcom_setsockopt(ota_https->sock_fd, SOL_SOCKET, SO_REUSEADDR, (char*)&reuse, sizeof(reuse));
        qcom_setsockopt(ota_https->sock_fd, SOL_SOCKET, SO_LINGER, (char *)&lin, sizeof(lin));
        qcom_setsockopt(ota_https->sock_fd, SOL_SOCKET, TCP_NODELAY, (char *)&yes, sizeof(yes));
        qcom_setsockopt(ota_https->sock_fd, SOL_SOCKET, SO_KEEPALIVE, (char *)&yes, sizeof(yes));

        if (!(ota_https->ssl_ctx = qcom_SSL_ctx_new(SSL_CLIENT, SSL_OTA_INBUF_SIZE, SSL_OUTBUF_SIZE, 0))) {
//                AT_PTF("SSL ctx new error\n");
                goto _alloc_error;
        }

        ota_https->hssl = qcom_SSL_new(ota_https->ssl_ctx);
        qcom_SSL_set_fd(ota_https->hssl, ota_https->sock_fd);
    }

    return ota_https;

_socket_error:
_alloc_error:
    _qcom_ota_https_close(ota_https);
    return 0;
}

A_INT32 qcom_ota_https_upgrade(
				char *			server,
                A_UINT32        ip_addr,
                A_UINT16        port,
                char            *filename,
                A_UINT32        *size)
{
    QCOM_OTA_HTTPS *ota_https = 0;
    A_INT32 ret = QCOM_OTA_OK;
    A_UINT32 partition_size;
	A_INT8	good_image = 0;

//    AT_PTF("Enter qcom_ota_https upgrade\n");
//    AT_PTF("ota_https_upgrade: ip:%xH, port:%d\n", ip_addr, port);
//    AT_PTF("                file:%s\n", filename);
//    AT_PTF("partition_index:%d \n", OTA_UPGRADE_PARTITION);


    if ((ret = qcom_ota_session_start(QCOM_OTA_TARGET_FIRMWARE_UPGRADE, 0/* OTA_UPGRADE_PARTITION *//* partition */ )) != QCOM_OTA_OK) {
		AT_PTF("ERROR-9.1");
        return ret;
    }

#if 0
    if ((ret = qcom_ota_partition_erase()) != QCOM_OTA_OK) {
        AT_PTF("OTA Partition Erase Fail\n");
        return ret;
    }
#endif

    if (!(partition_size = qcom_ota_partition_get_size())) {
		AT_PTF("ERROR-9.1");
        return QCOM_OTA_ERR_INVALID_PARTITION_INDEX;
    }

//    AT_PTF("OTA Partition Get Size: %d\n", partition_size);
    if (!(ota_https = _qcom_ota_https_open(server, ip_addr, port, filename, 1 /* flag */))) {
        ret = QCOM_OTA_ERR_INSUFFICIENT_MEMORY;
        goto _error;
    }

    ota_https->res->partition_size = partition_size;

    ret = _qcom_ota_https_request(ota_https, _qcom_ota_https_program, OTA_HTTPS_TIMEOUT, size);

    if (ret == QCOM_OTA_OK && *size > 0) {
        if ((ret = qcom_ota_partition_verify_checksum()) == QCOM_OTA_OK)  {
            good_image = 1;
//            AT_PTF("OTA Partition Verify Checksum is correct\n");
        } else {
//            AT_PTF("OTA Partition Verify Checksum is NOT correct (%d)\n", ret);
        }
    }

_error:
    _qcom_ota_https_close(ota_https);
    qcom_ota_session_end(good_image /* good image */ );


#if ENABLE_OTA_CHKSUM
	if (bIsChecksumDone == 1) {
    	qcom_ota_session_end(1 /* good image */ );
	} else {
    	qcom_ota_session_end(0 /* Upgrade not required */ );
	}
#endif

//    AT_PTF("Leave qcom_ota_https upgrade\n");
    bIsChecksumDone   = 0;
    return ret;
}

A_UINT32 _inet_addr(A_CHAR *str)
{
    A_UINT32 ipaddr;
    A_UINT32 data[4];
    A_UINT32 ret;

    ret = A_SSCANF(str, "%3d.%3d.%3d.%3d", data, data + 1, data + 2, data + 3);
    if (ret < 0) {
        return 0;
    } else {
        ipaddr = data[0] << 24 | data[1] << 16 | data[2] << 8 | data[3];
    }

    return ipaddr;
}

A_INT32 rdlr_ota_https_upgrade(uint32_t port, char * ip, char * filename)
{
    A_UINT32 ip_addr = 0;
    unsigned int sbits = 0;
    A_UINT32 resp_code;
    A_UINT32 size;

	if( (parse_ipv4_ad(&ip_addr, &sbits, ip) !=0)) {
		if (qcom_dnsc_get_host_by_name (ip, &ip_addr) != A_OK) {
			AT_PTF("ERROR-9.4");
			return ;
		}
	} else {
		ip_addr = _inet_addr(ip);
	}

    resp_code = qcom_ota_https_upgrade(ip, ip_addr, port, filename, &size);
    if (RDLR_OK != resp_code) {
//		AT_PTF("responce = %d\n", resp_code);
    } else {
		AT_PTF(RDLR_OK_STR);
    }
    return RDLR_OK;
}

