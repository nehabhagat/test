/*
  * Copyright (c) 2014 Qualcomm Atheros, Inc..
  * All Rights Reserved.
  * Qualcomm Atheros Confidential and Proprietary.
  */

#include "qcom_common.h"
#include "qcom_p2p_api.h"
#include "qcom_wps.h"
#include "qcom_uart.h"
#include "qcom_gpio.h"
#include "threadx/tx_api.h"
#include "threadxdmn_api.h"
#include "select_api.h"

A_INT32 uart1_fd;

#define ASCII_CARRIAGE_RETURN     0x0D
TX_THREAD host_thread;
#define BYTE_POOL_SIZE (5*1024)
#define PSEUDO_HOST_STACK_SIZE (4 * 1024)   /* small stack for pseudo-Host thread */
TX_BYTE_POOL pool;
extern void process_at_character(A_UINT8 character);

#define COMMAND_SIZE 512

void AT_PTF(char *,...);       //Our printf function
char* convert(int, int);       //Convert integer number into octal, hex, etc.

void reverse(char str[], int length)
{
    int i,j;
    char c;

    for (i = 0, j = length-1; i < j; ++i, --j) {
        c = str[i];
        str[i] = str[j];
        str[j] = c;
    }
}

void ftoa(double a, char *str2)
{
    int ab = 0;
    int ab_t = 0;
    int num;
    char str[20];
    char str1[7];
    int i = 0, j = 0;

    if (!str2) {
        return ;
    }
    ab = a;

    ab_t = (a - ab) * 1000000;

    num = ab;
    if (num == 0) {
        str[i++] = 48;
        goto next;
    }
    for (;num;num/=10) {
        str[i++] = (num%10) + 48;
    }
next:
    str[i] = '\0';
    reverse(str, strlen(str));
    str[i++] = '.';

    j = 0;
    num = ab_t;
    if (num == 0) {
        str1[j++] = 48;
        goto next_;
    }
    for (;num;num/=10) {
        str1[j++] = (num%10) + 48;
    }
next_:
    str1[j] = '\0';
    reverse(str1, strlen(str1));
    strcpy(str + i, str1);
    strcpy(str2, str);
}

void AT_PTF(char* format,...)
{
    char *traverse;
    long int i;
    char *s;
    unsigned u_val;
    char buf[1024];
    int index = 0;
    char float_str[20];
    A_UINT32 uart_len = 0;
    double f = 0.0;

    memset(float_str, 0, sizeof(float_str));

    va_list arg;
    va_start(arg, format);

    for(traverse = format; *traverse != '\0'; traverse++)
    {
        while( *traverse != '%' )
        {
            buf[index++] = *traverse;
            if (*traverse == '\0') {
                index--;
                break;
            }
            traverse++;
        }

        if (*traverse == '\0')
            break;

        traverse++;

        switch(*traverse)
        {
            case 'c' :  i = va_arg(arg,int);
                        buf[index++] = i;
                        break;

            case 'd' :  i = va_arg(arg,int);
                        if (i < 0)
                        {
                            i = -i;
                            buf[index++] = '-';
                        }
                        strcpy(buf + index, convert(i,10));
                        index += strlen(convert(i, 10));
                        break;

            case 'u' :  u_val = va_arg(arg, unsigned int);
                        strcpy(buf + index, convert(u_val, 10));
                        index += strlen(convert(u_val, 10));
                        break;

            case 'o' :  i = va_arg(arg,unsigned int);
                        strcpy(buf + index, convert(i, 8));
                        index += strlen(convert(i, 8));
                        break;

            case 'f' :  f = va_arg(arg, double);
                        ftoa(f, float_str);
                        strcpy(buf + index, float_str);
                        index += strlen(float_str);
                        break;

            case 's' :  s = va_arg(arg,char *);
                        strcpy(buf + index, s);
                        index += strlen(s);
                        break;

            case 'x' :  i = va_arg(arg,unsigned int);
                        strcpy(buf + index, convert(i, 16));
                        index += strlen(convert(i, 16));
                        break;

            case '%' :  buf[index++] = '%';
        }
    }

    va_end(arg);
//  uart_len = strlen(buf);
    uart_len = index;
    qcom_uart_write(uart1_fd, buf, &uart_len);
}

char *convert (int num, int base)
{
    static char Representation[]= "0123456789ABCDEF";
    static char buffer[50];
    char *ptr;

    ptr = &buffer[49];
    *ptr = '\0';

    do
    {
        *--ptr = Representation[num%base];
        num /= base;
    }while(num != 0);

    return(ptr);
}

void at_process_char(void)
{
    A_CHAR  character;
    A_UINT32      length;

    length = 1;

    character = 0;
    qcom_uart_read(uart1_fd, (A_CHAR *)&character, &length);

    if (length == 0) {
        return;
    }

    process_at_character(character);
}

void my_at_console_setup() {

    A_CHAR data_buf[512];
    A_UINT32 uart0_fd = 0;
    q_fd_set fd;
    struct timeval tmo;
    int ret = 0;

    memset(data_buf, 0, sizeof(data_buf));

    qcom_single_uart_init((A_CHAR *)"UART1");
    qcom_single_uart_init((A_CHAR *)"UART0");

    uart1_fd = qcom_uart_open((A_CHAR *)"UART1");
    if (uart1_fd == -1) {
        return;
    }
    uart0_fd = qcom_uart_open((A_CHAR *)"UART0");
    if (uart1_fd == -1) {
        return;
    }


    while (1) {

        FD_ZERO(&fd);
        FD_SET(uart1_fd, &fd);
        tmo.tv_sec = 30;
        tmo.tv_usec = 0;

        ret = qcom_select(uart1_fd + 1, &fd, NULL, NULL, &tmo);
        if (ret == 0) {
            continue;
        } else {
            if(FD_ISSET(uart1_fd, &fd)) {
                at_process_char();
            }
        }
    }
}

void
shell_host_entry(ULONG which_thread)
{

    extern void user_pre_init(void);
    user_pre_init();

    qcom_gpio_pin_dir(1, 0);
    qcom_gpio_pin_set(1, 1);
    qcom_thread_msleep (500);

    //qcom_enable_print(1);
    my_at_console_setup();

    /* Used for Controller*/
}


void user_main(void)
{
    extern void task_execute_cli_cmd();
    tx_byte_pool_create(&pool, "cdrtest pool", TX_POOL_CREATE_DYNAMIC, BYTE_POOL_SIZE);

    {
        CHAR *pointer;
        tx_byte_allocate(&pool, (VOID **) & pointer, PSEUDO_HOST_STACK_SIZE, TX_NO_WAIT);

        tx_thread_create(&host_thread, "cdrtest thread", shell_host_entry,
                         0, pointer, PSEUDO_HOST_STACK_SIZE, 16, 16, 4, TX_AUTO_START);
    }

    cdr_threadx_thread_init();
}

