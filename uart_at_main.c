/*
 * Copyright (c) 2015 Qualcomm Atheros, Inc.
 * All Rights Reserved.
 * Qualcomm Atheros Confidential and Proprietary.
 * $ATH_LICENSE_TARGET_C$
 */

#include "qcom_common.h"
#include "qcom_uart.h"
#include "qcom_internal.h"
#include "qcom/qcom_cli.h"
#include "qcom_ssl.h"
#include "qcom_utils.h"
#include "qcom/socket_api.h"
#include "qcom/select_api.h"
#include "qcom/qcom_wps.h"
#include "qcom/qcom_gpio.h"
#include "qcom/qcom_adc_api.h"
#include "qcom/qcom_network.h"
#include "qcom/qcom_nvram.h"

//#include "aj_target_nvram.h"
#include "qcom/qcom_dset.h"
#include "dsetid.h"
#include "dset_api.h"

extern A_INT32 uart1_fd;
void AT_PTF(char* format,...);
void reverse(char str[], int length);
void ftoa(double a, char *str2);
void rdlr_get_battery_percentage (char *battery_life);

#define HTC_RAM_TEST_PATCH

#define AT_WMI_COMMAND                      "AT+SET_STAGE    ="
#define AT_WMI_COMMAND_SIZE                 (sizeof(AT_WMI_COMMAND) - 1)
#define AT_WMI_END_POINT                    0x01
#define AT_WMI_TIMEOUT                      2000
#define HTC_HEADER_PAD                      ((AT_WMI_COMMAND_SIZE & 0x03) % 4)
#define MAX_TCP_CLI                         6
#define DEFAULT_PING_SIZE                   64

#define FLASH_SECTOR_SIZE                   4096
#define QBC_TICKET_PARAM_ADDR               0x00100000
#define QBC_TICKET_INFO_ADDR                0x00101001      //  100 + 4096(sector size) + 1
#define QBC_CHKSUM_ADDR                     0x000c0A00
#define QBC_SAVE_FLAG                       0x55AA55AA
#define QBC_OTA_FLAG                        0xB51B51B5

#define RDLR_OK                             0
#define RDLR_CMD_CODE                       1
#define RDLR_DATA_CODE                      2
#define RDLR_ABRT_CODE                      3
#define RDLR_INVALID_CMD                    4
#define RDLR_INET_OK                        8
#define RDLR_STA_LOCAL_OK                   9
#define RDLR_SOCK_LS_ERROR                  25
#define RDLR_TCP_CLI_ERROR                  30
#define RDLR_INVALID_SERVER                 31
#define RDLR_TCP_SRVR_ERROR                 35
#define RDLR_CONNECTED                      38
#define RDLR_DISCONNECTED                   39
#define RDLR_MODE                           44
#define RDLR_INIT_MODE                      52
#define RDLR_TCP_CLOSED                     59
#define RDLR_AP_UP                          72
#define RDLR_AP_CLI_CONNECT                 73
#define RDLR_STA_INET_STATUS                74

#define AP_INIT_OK                          00
#define AP_STATE_UP                         01
#define AP_STA_OK                           02

#define DHCP_WAIT                           1000
#define AP_WAIT                             100
#define WIFI_CONNECT_TIMEOUT                (60 * 1000)
#define WIFI_CONNECT_SLEEP                  (10000)
#define RDLR_IPA                            0xC0A80401
#define RDLR_START_IP                       0xC0A80464
#define RDLR_END_IP                         0xC0A80494
#define RDLR_IP_LEASE                       (60 * 60* 1000)
#define RDLR_MASK                           0xFFFFFF00
#define RDLR_GW                             0x00000000
#define RDLR_CREDIT_SIZE                    1500
#define RDLR_PASSWORD                       "12345678"
#define RDLR_CONSOLESLEEP                   1000
#define RDLR_TX_BUFFER_FAIL                 -100
#define RDLR_OK_STR                         "\"OK\""



#define MAX_TICKET                          100
#define RDLR_RECEIVE_DATA_LENGTH            160
#define RDLR_META_DATA_LENGTH               125
#define RDLR_WLAN_VERSION                   0x0003
#define RDLR_RESPONCE_NUM                   900000000

#define RDLR_SERVER_PORT                    "3367"
#define RDLR_AP_CHANNEL                     6

#define RDLR_PWM0_GPIO6                     6   /* PIN NAME - PWM0 */
#define RDLR_PWM2_GPIO8                     8   /* PIN NMAE - PWM2 */

#define WAKE_MGR_WAKE_EVENT_TIMER           0x00000001
#define WAKE_MGR_WAKE_EVENT_GPIO30_HIGH     0x00000002
#define WAKE_MGR_WAKE_EVENT_GPIO30_LOW      0x00000004
#define WAKE_MGR_WAKE_EVENT_GPIO31_HIGH     0x00000008
#define WAKE_MGR_WAKE_EVENT_GPIO31_LOW      0x00000010

#define OTA_UPGRADE_PARTITION               0

// Reference Data
// #define RDLR_TICKET "55,5,5,RIDLR,0440,001,002,1,0,0,00,M,0000000,0000000,0000000,0000000,0000000,0000000,0000000,0000000000000000,P,PS,0000000000000000,ddmmyy,01,001,002,\r\n"

A_INT32 rdlr_ota_https_upgrade(uint32_t port, char * ip, char * filename);

typedef struct {
    int l_onoff;
    int l_linger;
} RDLR_Linger;

A_UINT32  rdlr_timeout      = WIFI_CONNECT_TIMEOUT;
A_UINT32  pinginetIp        = 0x08080808;

typedef PREPACK struct _HTC_FRAME_HDR{
    /* do not remove or re-arrange these fields, these are minimally required
     * to take advantage of 4-byte lookaheads in some hardware implementations */
    A_UINT8   EndpointID;
    A_UINT8   Flags;
    A_UINT16  PayloadLen;       /* length of data (including trailer) that follows the header */

    /***** end of 4-byte lookahead ****/

    A_UINT8   ControlBytes[2];

    /* message payload starts after the header */

} POSTPACK HTC_FRAME_HDR;

extern void _WMI_Dispatch(unsigned short commandID,unsigned char *pDataBuffer,int dataSize,unsigned short dataInfo);
static int  s_bDumpData = FALSE;

A_UINT8  currentDeviceId    = 0;
A_UINT8  gdns_cli_enable    = 0;
A_UINT8  g_connect_state    = 0;
A_UINT8  dhcpc_check        = 0;
A_UINT8  rdlr_init_progress = 0;
A_UINT8  inetEnabled        = 0;
A_UINT8  apState            = AP_INIT_OK;
A_UINT8  station_count      = 0;

typedef PREPACK struct ticket_param
{
    int issued_tickets_count;
    int flag;
    A_UINT32 ticket_offset;
}T_PARAM;
T_PARAM  t_param;

typedef PREPACK struct ticket_info
{
    char ticket_raw[RDLR_RECEIVE_DATA_LENGTH];
}T_INFO;
T_INFO t_info;

A_UINT32 ticket_offset = 0;

char ticket_raw[RDLR_RECEIVE_DATA_LENGTH + 100];
char metadata_buffer[RDLR_META_DATA_LENGTH];        //  Needs validation after solving sta_get_info Bug......
char ticket_response[128] = "01,9916002132,25:10:16;15:30:56,000000123456,0987654321,12345678\r\n";
A_UINT8 station_mac_arr[MAX_TCP_CLI][6];
A_UINT32 station_ip_addr[MAX_TCP_CLI];

unsigned char digest[32];
SSL_INST ssl_inst[MAX_SSL_INST];
SSL_INST *ssl = NULL;

#if 0
/* TODO: Would include wmi.h, but there are header issues */
typedef PREPACK sAX_TCP_CLIuct {
    A_UINT16    commandID;
    A_UINT16    commandInfo;
    A_UINT16    reserved;      /* For alignment */
} POSTPACK WMI_CMD_HDR;        /* used for commands and events */
#endif

typedef PREPACK struct {
    A_UINT32    commandID;
} POSTPACK WMIX_CMD_HDR;

/* uart at mode */
typedef enum {
    UART_AT_CMD_MODE = 0,
    UART_AT_DATA_MODE
} UART_AT_OPERATING_MODE_T;

/* AT CMD type */
typedef struct {
    char*       cmd_str;
    A_UINT16    cmd_str_len;
    A_UINT8     min_args;
    A_UINT8     can_exec_in_unconn_state;
    A_UINT8     can_exec_in_conn_state;
    void (* process_cmd)(char* cmd_str, A_UINT16 cmd_len);
    char*       help_str;
} AT_CMD_ENTRY;

UART_AT_OPERATING_MODE_T g_operating_mode;

/* important buffer and offset */
char *at_buffer;    /* buffer for cmd from uart */
A_UINT8 *data_buffer;  /* buffer for data from uart */

int cmd_offset  = 0; /* offset in cmd buffer */
int data_offset = 0;/* offset in data buffer */
int g_data_buffer_size = 512; //data buffer size for uart data rx, default 512.
#define AT_CMD_BUFFER_LEN 1664

/* data of tcp/udp socket */
int tcp_socket       = 0;
int tcp_cli_socket   = 0;
int tcp_cli[4]       = {0};

A_INT32 recvBuffSize = 1024; //data buffer size for socket data rx, default 1024.
A_UINT8 *pDataBuffer = NULL;

int g_tx_timeout = 300;  //timeout in tx
A_UINT32 g_Baud  = 115200; // store the baud for query

/* params of mode switch */
A_UINT8 plus_received = 0;
A_UINT8 txdata_init   = 0;
A_UINT8 mqdata_init   = 0;
unsigned long plus_last_time;
#define PLUS_TIME_INTERVAL 1000

/* function of tasks */
extern int  qcom_task_start(void (*fn) (unsigned int), unsigned int arg, int stk_size, int tk_ms);
extern void qcom_task_del(void);
extern void qcom_task_exit(void);

/* specific cmd processing function */
void process_WD(char* cmd_str, A_UINT16 cmd_len);
void process_HELP(char* cmd_str, A_UINT16 cmd_len);
void process_RDLR_SetSrvr(char* cmd_str, A_UINT16 cmd_len);
void process_RDLR_SocketClose (char* cmd_str, A_UINT16 cmd_len);
void process_Reset (char* cmd_str, A_UINT16 cmd_len);
void process_Ping (char* cmd_str, A_UINT16 cmd_len);
void process_RDLR_METADATA (char* cmd_str, A_UINT16 cmd_len);
void process_RDLR_SETTRIP (char* cmd_str, A_UINT16 cmd_len);
void process_RDLR_SHUTDOWN (char* cmd_str, A_UINT16 cmd_len);
void process_RDLR_GETDATA (char* cmd_str, A_UINT16 cmd_len);
void process_RDLR_CHECK (char* cmd_str, A_UINT16 cmd_len);
void process_RDLR_VALIDATE (char* cmd_str, A_UINT16 cmd_len);
void process_RDLR_SETSTAGE (char* cmd_str, A_UINT16 cmd_len);
void process_RDLR_GPIO (char* cmd_str, A_UINT16 cmd_len);
void process_RDLR_GIP (char* cmd_str, A_UINT16 cmd_len);
void process_RDLR_CWMODE(char* cmd_str, A_UINT16 cmd_len);
void process_RDLR_ADC (char* cmd_str, A_UINT16 cmd_len);
void process_RDLR_PSUS (char* cmd_str, A_UINT16 cmd_len);
void process_RDLR_OTA(char* cmd_str, A_UINT16 cmd_len);
void process_RDLR_MAC(char* cmd_str, A_UINT16 cmd_len);
void process_RDLR_Erase(char* cmd_str, A_UINT16 cmd_len);
void wifi_conn_callback(A_UINT8 device_id, int value);

/* help infos */
#define RDLR_HELP   "Refer Rdlr Hostless API documentation for reference\n\r"

/* cmd Engine */
AT_CMD_ENTRY at_cmds[] = {
     /* CMD              LEN                     args exec u?, exec c? process_cmd                     help   */
    { "RPING=",         sizeof("RPING=")-1,      0,      0,    1,     process_Ping,                 RDLR_HELP},

    { "AT+PSUS=",       sizeof("AT+PSUS=")-1,          0,      0,    1,     process_RDLR_PSUS,          RDLR_HELP},

    /********************************   Commands For Rdlr Version    ******************************************/
    { "AT+METADATA",    sizeof("AT+METADATA")-1,       0,      0,    1,     process_RDLR_METADATA,      RDLR_HELP},
    { "AT+SHUTDOWN",    sizeof("AT+SHUTDOWN")-1,       0,      0,    1,     process_RDLR_SHUTDOWN,      RDLR_HELP},
    { "AT+CHECK",       sizeof("AT+CHECK")-1,          0,      0,    1,     process_RDLR_CHECK,         RDLR_HELP},
    { "AT+RESET",       sizeof("AT+RESET")-1,          0,      0,    1,     process_Reset,              RDLR_HELP},
    { "AT+SET_TRIP=",   sizeof("AT+SET_TRIP=")-1,      0,      0,    1,     process_RDLR_SETTRIP,       RDLR_HELP},
    { "AT+GET_DATA=",   sizeof("AT+GET_DATA=")-1,      0,      0,    1,     process_RDLR_GETDATA,       RDLR_HELP},
    { "AT+VALIDATE=",   sizeof("AT+VALIDATE=")-1,      0,      0,    1,     process_RDLR_VALIDATE,      RDLR_HELP},
    { "AT+SET_STAGE=",  sizeof("AT+SET_STAGE=")-1,     0,      0,    1,     process_RDLR_SETSTAGE,      RDLR_HELP},
    { "AT+OTA=",        sizeof("AT+OTA=")-1,           0,      0,    1,     process_RDLR_OTA,           RDLR_HELP},
    { "AT+CWMODE=",     sizeof("AT+CWMODE=")-1,        0,      0,    1,     process_RDLR_CWMODE,        RDLR_HELP},
    /******************************************** END ***********************************************************/

    { "GPIO=",          sizeof("GPIO=")-1,             0,      0,    1,     process_RDLR_GPIO,          RDLR_HELP},
    { "MAC=",           sizeof("MAC=")-1,              0,      0,    1,     process_RDLR_MAC,           RDLR_HELP},
    { "GIP=",           sizeof("GIP=")-1,              0,      0,    1,     process_RDLR_GIP,           RDLR_HELP},
    { "ADC=",           sizeof("ADC=")-1,              0,      0,    1,     process_RDLR_ADC,           RDLR_HELP},
    { "ER=",            sizeof("ER=")-1,               0,      0,    1,     process_RDLR_Erase,         RDLR_HELP},
    { "RHELP=",         sizeof("RHELP=")-1,            0,      1,    0,     process_HELP,               RDLR_HELP},
    { NULL,             0,                             0,      0,    0,     NULL,                       NULL},
};

/*
 * Util Fuctions
 */
int my_strncmpi(char *str1, char *str2, int length)
{
    char temp_str1, temp_str2;
    if (length <= 0)
        return 0;

    while (--length >= 0)
    {

        //convert both characters to uppercase
        temp_str1 = *str1;
        if((*str1 >= 0x61) && (*str1 <= 0x7a))
        {
            temp_str1 = *str1 - 0x20;
        }

        temp_str2 = *str2;
        if((*str2 >= 0x61) && (*str2 <= 0x7a))
        {
            temp_str2 = *str2 - 0x20;
        }

        if((temp_str1 != temp_str2) || (temp_str1 == '\0'))
        {
            break;
        }

        ++str1;
        ++str2;
    }

    return (temp_str1 - temp_str2);
}

int parse_ipv4_ad(unsigned int * ip_address,   /* pointer to IP address returned */
        unsigned *  sbits,      /* default subnet bit number */
        char *   stringin)
{
    int error = -1;
    char *   cp;
    int   dots  =  0; /* periods imbedded in input string */
    int   number;
    union
    {
        unsigned char   c[4];
        unsigned long   l;
    } retval;

    cp = stringin;
    while (*cp == ' ')
      ++cp;
    while (*cp)
    {
        if (*cp > '9' || *cp < '.' || *cp == '/')
            return(error);
        if (*cp == '.')dots++;
        cp++;
    }

    if ( dots < 1 || dots > 3 )
        return(error);

    cp = stringin;
    if ((number = atoi(cp)) > 255)
        return(error);

    retval.c[0] = (unsigned char)number;

    while (*cp != '.')cp++; /* find dot (end of number) */
    cp++;             /* point past dot */

    if (dots == 1 || dots == 2) retval.c[1] = 0;
    else
    {
        number = atoi(cp);
        while (*cp != '.')cp++; /* find dot (end of number) */
        cp++;             /* point past dot */
        if (number > 255) return(error);
        retval.c[1] = (unsigned char)number;
    }

    if (dots == 1) retval.c[2] = 0;
    else
    {
        number = atoi(cp);
        while (*cp != '.')cp++; /* find dot (end of number) */
        cp++;             /* point past dot */
        if (number > 255) return(error);
        retval.c[2] = (unsigned char)number;
    }

    if ((number = atoi(cp)) > 255)
        return(error);
    retval.c[3] = (unsigned char)number;

    if (retval.c[0] < 128) *sbits = 8;
    else if(retval.c[0] < 192) *sbits = 16;
    else *sbits = 24;

    *ip_address = retval.l;
    return(0);
}

int copy_data_upto(void* from_buf, void* to_buf, A_UINT16 * cmd_sz_read, A_UINT16 * tSz, A_UINT8 remove_leading_zeros, char term)
{
    A_UINT8* vpBuffer = (A_UINT8*) from_buf;
    A_UINT8* rpBuffer = (A_UINT8*) to_buf;
    //A_UINT8 leadingZero = 1;
    A_UINT8 leadingZero = remove_leading_zeros;
    A_UINT16 tMax = *tSz;
    *cmd_sz_read = 0;
    *tSz = 0;

    while(vpBuffer[*cmd_sz_read] != term)
    {
        if(vpBuffer[*cmd_sz_read] == '\r') //Unexpected '\r' before reading upto term char
        {
            return 0;
        }

        if(leadingZero && (vpBuffer[*cmd_sz_read] == '0'))
        {
            (*cmd_sz_read)++;
            continue;
        }
        else
        {
            leadingZero = 0;
            rpBuffer[*tSz] = vpBuffer[*cmd_sz_read];
            (*tSz)++;
            (*cmd_sz_read)++;
        }

        if((*tSz) >= tMax)
            return 0;
    }
    /* The argument was zero. Just send it */
    if(!(*tSz) && leadingZero)
    {
        rpBuffer[0] = '0';
        *tSz =1;
        return 0;
    }
    return 1;
}

/* function to switch mode : data <-->cmd */
void switch_uart_mode(int cmdMode)
{
    /* data --> cmd */
    if (cmdMode == 1){
        g_operating_mode = UART_AT_CMD_MODE;
        //AT_PTF("R=%d--!", RDLR_CMD_CODE);
    /* cmd -->data */
    } else {
        if(!data_buffer){
            data_buffer = qcom_mem_alloc(g_data_buffer_size );
            memset(data_buffer,0,g_data_buffer_size);
        }

        g_operating_mode = UART_AT_DATA_MODE;
        //AT_PTF("R=%d--!", RDLR_DATA_CODE);
    }
}

/* process the cmd here */
void process_cmd()
{

    char* cmd_str ;
    int   cmd_index = 0;
    int   cmd_found = 0;
    int   len = cmd_offset;

    if (!at_buffer) //this should not happen
    {
//        AT_PTF("R=%d--!", RDLR_ABRT_CODE);
        return;
    }
    cmd_str =  (char*)(at_buffer);

    //Process the command here
    if(1)
    {
        while((at_cmds[cmd_index].cmd_str) != NULL)
        {
            if(my_strncmpi(cmd_str, at_cmds[cmd_index].cmd_str, at_cmds[cmd_index].cmd_str_len) == 0)
            {
//              AT_PTF(at_cmds[cmd_index].cmd_str);
                cmd_str+=at_cmds[cmd_index].cmd_str_len;
                len-= at_cmds[cmd_index].cmd_str_len;
                AT_PTF("\n\r");
                cmd_found++;
                at_cmds[cmd_index].process_cmd(cmd_str, len);
                break;
            }
            cmd_index++;
        }

        if(!cmd_found)
        {
//          AT_PTF("Command Not Found\n");
            AT_PTF("ERROR-16.0");
        }
    }
}

//entry of cmd character
/* insert the char into cmd buffer */
static unsigned int s_currentTime;
static int          s_bCurrentTimeSet = FALSE;

void CMD_MODE_append_char(char c)
{
  unsigned short commandSize;
  char          *pError;
  char          *pHtcFrameHeader;

  if (cmd_offset >= AT_WMI_COMMAND_SIZE &&
      my_strncmpi(at_buffer, AT_WMI_COMMAND, AT_WMI_COMMAND_SIZE) == 0) {
    pError = "*Timeout waiting for HTC and WMI header";
    if (cmd_offset == AT_WMI_COMMAND_SIZE) {
      cmd_offset += HTC_HEADER_PAD;         /* HTC header on 4-byte boundary */

      /*-----------------------------------------------------------------
      ; 'D' ==> Special character to turn ON  debug
      ; 'X' ==> Special character to turn OFF debug
      ;----------------------------------------------------------------*/
      if (c == 'D' || c == 'X') {
        pError = NULL;
        s_bDumpData = TRUE;
        if (c == 'X')
          s_bDumpData = FALSE;
      }
    }
    if (cmd_offset < AT_CMD_BUFFER_LEN - 1) {
        AT_PTF("->");

        *((char *)at_buffer + cmd_offset) = c;
        ++cmd_offset;
    }
    if (s_bCurrentTimeSet == FALSE) {
        s_bCurrentTimeSet = TRUE;
        s_currentTime = time_ms();
    }
    commandSize = 0;
    if (pError != NULL &&
        cmd_offset >= AT_WMI_COMMAND_SIZE + sizeof(HTC_FRAME_HDR) + sizeof(WMI_CMD_HDR) + HTC_HEADER_PAD) {
        pHtcFrameHeader = at_buffer + AT_WMI_COMMAND_SIZE + HTC_HEADER_PAD;
        commandSize = ((HTC_FRAME_HDR *)pHtcFrameHeader)->PayloadLen;
        if (commandSize >= sizeof(WMI_CMD_HDR) &&
           commandSize < AT_CMD_BUFFER_LEN - HTC_HEADER_PAD - AT_WMI_COMMAND_SIZE - sizeof(HTC_FRAME_HDR) &&
           ((HTC_FRAME_HDR *)pHtcFrameHeader)->EndpointID == AT_WMI_END_POINT) {
           pError = "*Timeout waiting for WMI data";
           if (cmd_offset == HTC_HEADER_PAD + AT_WMI_COMMAND_SIZE + sizeof(HTC_FRAME_HDR) + commandSize) {
               pError = NULL;
               //process_WMI(at_buffer + AT_WMI_COMMAND_SIZE + HTC_HEADER_PAD + sizeof(HTC_FRAME_HDR), commandSize);
           }
        }
        else {
            pError = NULL;
        }
    }
    if (pError == NULL || time_ms() - s_currentTime > AT_WMI_TIMEOUT) {
      memset(at_buffer, 0, AT_CMD_BUFFER_LEN);
      cmd_offset = 0;
      s_bCurrentTimeSet = FALSE;
      AT_PTF("\n\rRDLR>");
    }
    return;
  }

    if((c =='\n') || (c == '\r') || (cmd_offset >= (AT_CMD_BUFFER_LEN -1)))
    {
        if(cmd_offset)
        {
            //terminate command string with NULL character
            *( (char*)at_buffer + cmd_offset) ='\0';

//          AT_PTF(at_buffer);
            process_cmd();
        }
        else
        {
            AT_PTF("\n\rRDLR>");
        }

        memset(at_buffer,0,AT_CMD_BUFFER_LEN);
        cmd_offset = 0;

    }
    else
    {
        if(cmd_offset < AT_CMD_BUFFER_LEN-1)
        {
            *((char *)at_buffer + cmd_offset) = c;
            cmd_offset++;
        }
    }
}

/*************************************************************
 * Function     : at_send_packet
 * Description  : this function is sending the data in the buffer by socket.
 *************************************************************/
void at_send_packet(){

    int      i, ret = 0, fd = 0;
    uint32_t ping_dns = 0x08080808;
    uint32_t  devmode  = 0;

    /* run in data mode only */
    if (g_operating_mode == UART_AT_DATA_MODE) {
        /* if there is data in buffer, send it */
        if (data_offset){

        if (tcp_socket && txdata_init) {
                for (i = 0; i < MAX_TCP_CLI; i++) {
            fd = tcp_cli[i];
            if (fd <= 0) {
                continue;
            }
            ret = qcom_send (fd, (char *)data_buffer, data_offset, 0);

            if (ret == RDLR_TX_BUFFER_FAIL) {
                        qcom_thread_msleep (100);
                ret = qcom_send (fd, (char *)data_buffer, data_offset, 0);
            }

            if ( ret  < 0 ) {
                qcom_socket_close(fd);
                tcp_cli[i] = 0;
//                        AT_PTF("R=%d--!",RDLR_TCP_SRVR_ERROR);
            }
            qcom_thread_msleep (100);
        }

        qcom_op_get_mode(currentDeviceId, &devmode);
        if (devmode == QCOM_WLAN_DEV_MODE_STATION && !inetEnabled) {
                ret = qcom_ping (ping_dns, DEFAULT_PING_SIZE);
                if (ret == A_OK) {
                    inetEnabled = 1;
//                       AT_PTF("R=%d--!", RDLR_STA_INET_STATUS);
                }
            }

        }

        ret = 0;
        if (tcp_cli_socket && mqdata_init) {
            if (inetEnabled) {
                ret = qcom_ping (ping_dns, DEFAULT_PING_SIZE);
            }
            if (ret == A_OK) {
                if (ssl == NULL) {
                        ret = qcom_send(tcp_cli_socket, (char *)data_buffer, data_offset, 0);
            } else {
                if (ssl->ssl) {
                           ret = qcom_SSL_write (ssl->ssl, (char *) data_buffer, data_offset);
                }
            }
            qcom_thread_msleep (100);

            if (ret < 0) {
//                       AT_PTF("R=%d--!", RDLR_TCP_CLI_ERROR);
            }
        } else {
//                    AT_PTF("R=%d--!", RDLR_TCP_CLI_ERROR);
        }
    }

    memset(data_buffer, 0, g_data_buffer_size);
    data_offset = 0;
    txdata_init = 0;
    mqdata_init = 0;

    // switch to command mode
    switch_uart_mode (1);
    }

    }

}

/* insert char or send data or receive + */
void insert_data_into_buffer(char c, A_UINT8 plus){
    switch(plus){
        case 0:
            if((data_offset > (g_data_buffer_size -1)))
            {
                at_send_packet();
                insert_data_into_buffer(c,0);
            }else
            {
                *((char *)data_buffer + data_offset) = c;
                data_offset++;
            }
            break;
        case 1: case 2:
            at_send_packet();
            break;
        case 3:
            while(plus_received--){
                *((char *)data_buffer + data_offset) = '+';
                data_offset++;
            }
            break;
        default:
            break;
    }
}

/* entry of data character */
/* insert data character into data buffer to tx */
void DATA_MODE_append_char(char c, A_UINT8 timeout)
{
    unsigned long current_time;

    /*  check if it is mode switch */
    /* to switch to cmd mode from data mode , sequence of +++ is expected with a minimum pause period of 1sec between the + */

    /* +++ or +* */
    if(plus_received){
        if(c=='+' && (( current_time=time_ms() ) - plus_last_time) >= PLUS_TIME_INTERVAL){
            /* AT_PTF("time of  second and ... + is: %d.\n", current_time); */
            plus_last_time = current_time;
            plus_received++;
            if(plus_received ==3){
                /* switch to cmd mode */
                //AT_PTF("R=RDLR_SWITCH\n");
                switch_uart_mode(1);
            }
        }else{
            insert_data_into_buffer('+',3);
            plus_received =0;
            plus_last_time =0;
            goto insert_data;
        }
    }

    /* here we process the fisrt '+' */
    /* record the time the first '+' */
    if( (c =='+') && (!plus_received)){
        current_time = time_ms();
        /* AT_PTF("time of + is: %d.\n", current_time); */
        plus_last_time = current_time;
        plus_received++;
    }

insert_data:
    /*  fill the character into the buffer */
    /* user can set this buffer size value */
    insert_data_into_buffer(c,plus_received);

}

/* entry of character from uart*/
void process_at_character(A_UINT8 character){

    //molloc the cmd buffer
    if (!at_buffer) {
        at_buffer = qcom_mem_alloc(AT_CMD_BUFFER_LEN);
        memset(at_buffer, 0, AT_CMD_BUFFER_LEN);
    }

    if (g_operating_mode == UART_AT_DATA_MODE) {
//        DATA_MODE_append_char(character, 0);
    } else {
        CMD_MODE_append_char(character);
    }
}

void process_WD (char* cmd_str, A_UINT16 cmd_len)
{
    if (rdlr_init_progress == 1 && cmd_str != NULL) {
//        AT_PTF("R=%d--!",RDLR_INIT_MODE);
    return ;
    }

    qcom_disconnect (currentDeviceId);
}

void process_Reset (char* cmd_str, A_UINT16 cmd_len)
{
    qcom_sys_reset();
}

void process_Ping (char* cmd_str, A_UINT16 cmd_len) {

    unsigned short token_len;
    A_UINT32 addr;
    char token_buf[32];
    unsigned int sbits = 0;
    A_UINT16 ping_size;
    int ret = 0;

    do{

        A_MEMSET(token_buf, 0, 32);
        token_len = (cmd_len<32) ? cmd_len : 31; //maximum len for this token
        copy_data_upto(cmd_str, token_buf, &cmd_len, &token_len, 1, ',');

        ping_size = DEFAULT_PING_SIZE;

        ret = parse_ipv4_ad(&addr, &sbits, (char*)token_buf);
        if (ret != 0) {
            if (qcom_dnsc_get_host_by_name (token_buf, &addr) != A_OK) {
//                AT_PTF("R=%d--!",RDLR_INVALID_SERVER);
                break;
            }
        } else {
            addr = htonl(addr);
        }

        if (qcom_ping(addr, ping_size) == A_OK) {
            AT_PTF("OK\n");
        } else {
            AT_PTF("NO-INET\n");
        }

        token_len++; //skip ','
        cmd_str+=token_len;
        cmd_len-=token_len;

    }while(0);
}

A_INT32 flash_erase(A_UINT32 addr, A_UINT32 size) {

    A_INT32 ret = 0;
    A_INT8 erase_count = 0;
    A_INT32 len_t;

    if (!addr || !size) {
        return -1;
    }

    AT_PTF("size = %d\n", size);

    len_t = size;

    while (len_t > 0) {
        len_t -= FLASH_SECTOR_SIZE;
        erase_count++;
    }

    AT_PTF("erase_count = %d\n", erase_count);

    ret = qcom_nvram_erase(addr, FLASH_SECTOR_SIZE * erase_count);

    return ret;
}

// FLASH CMDS
void tickets_param_save_flash (T_PARAM *param)
{
    param->flag = QBC_SAVE_FLAG;
    flash_erase(QBC_TICKET_PARAM_ADDR, sizeof(T_PARAM));
    qcom_nvram_write(QBC_TICKET_PARAM_ADDR,
                    (unsigned char *)param, sizeof(T_PARAM));
}

int tickets_param_load_flash (T_PARAM *param) {

    qcom_nvram_read(QBC_TICKET_PARAM_ADDR,
                    (unsigned char *)param, sizeof(T_PARAM));
    if (param->flag == QBC_SAVE_FLAG) {
        return 1;
    } else {
        return -1;
    }
}

// FLASH CMDS
void tickets_info_save_flash (T_INFO *param)
{
    qcom_nvram_write(QBC_TICKET_INFO_ADDR + ticket_offset,
                        (unsigned char *)param, sizeof(T_INFO));
}

int tickets_info_load_flash (T_INFO *param, A_UINT32 offset) {

    qcom_nvram_read(QBC_TICKET_INFO_ADDR + offset,
                        (unsigned char *)param, sizeof(T_INFO));
}

char* itoa(int num, char* str, int base)
{
    int i = 0;
    int isNegative = 0;

    /* Handle 0 explicitely, otherwise empty string is printed for 0 */
    if (num == 0)
    {
        str[i++] = '0';
        str[i] = '\0';
        return str;
    }

    // In standard itoa(), negative numbers are handled only with
    // base 10. Otherwise numbers are considered unsigned.
    if (num < 0 && base == 10)
    {
        isNegative = 1;
        num = -num;
    }

    // Process individual digits
    while (num != 0)
    {
        int rem = num % base;
        str[i++] = (rem > 9)? (rem-10) + 'a' : rem + '0';
        num = num/base;
    }

    // If number is negative, append '-'
    if (isNegative)
        str[i++] = '-';

    str[i] = '\0'; // Append string terminator

    // Reverse the string
    reverse(str, i);

    return str;
}

int ticket_process(int fd, A_UINT8 * in_buffer, int length)
{

    T_PARAM *param = &t_param;
    T_INFO *param_i = &t_info;
    char buffer[20];
    int ret = 0;
    int zero_length = 0;
    int i = 0, j = 0;
    int response_len = 0;

    if (fd <= 0) {
        return -1;
    }
    memset(param, 0, sizeof(T_PARAM));
    memset(param_i, 0, sizeof(T_INFO));
    ret = tickets_param_load_flash(param);

    if (ret == 1) {
        ticket_offset = param->ticket_offset;
    } else {
        memset(param, 0, sizeof(T_PARAM));
    }

    if (param->issued_tickets_count >= MAX_TICKET) {                // Rollback to Zero + Erase the Flash upto available count and parameters
        AT_PTF("in ticket_process\n");
        flash_erase(QBC_TICKET_INFO_ADDR, param->ticket_offset);
        flash_erase(QBC_TICKET_PARAM_ADDR, sizeof(T_PARAM));
    }

    if (length > 0) {
        in_buffer[length++] = '\0';

        memset(buffer, 0, sizeof(buffer));
        itoa(param->issued_tickets_count + 1, buffer, 16);
        j = 0;
        zero_length = 4 - strlen(buffer);
        while (zero_length--) {
            param_i->ticket_raw[j++] = '0';
        }
        i = 0;
        while (i < (4 - zero_length)) {
            param_i->ticket_raw[j] = buffer[i];
            i++;
            j++;
        }
        j = 0;
        while(!(in_buffer[j++] == ','));

        i = 4;
        strcpy(param_i->ticket_raw + i, (char *)(in_buffer + j - 1));
        AT_PTF("in ticket_process ticket = %s\n", param_i->ticket_raw);

        param->issued_tickets_count++;
        param->ticket_offset += sizeof(T_INFO);
        itoa(param->issued_tickets_count + RDLR_RESPONCE_NUM, buffer, 10);
        for (i = 4; i<=12; i++) {
            ticket_response[i] = buffer[i-4];
        }
        response_len = strlen(ticket_response) + 1;

        ret = qcom_send (fd, ticket_response, response_len, 0);
        if (ret == RDLR_TX_BUFFER_FAIL) {
            qcom_thread_msleep (100);
            ret = qcom_send (fd, ticket_response, response_len, 0);
        }
        if (ret < 0) {
            return -1;
        }
        tickets_param_save_flash(param);
        tickets_info_save_flash(param_i);
        switch_uart_mode (1);   // Going into Data Mode
        return 0;
    }
}

void at_tcp_server_receive(A_UINT32 param_unused)
{

    A_INT32 recvBytes               = 0;
    A_INT32 fdAct                   = 0;
    A_INT32 fromSize                = 0;
    A_INT8  rdlr_credential_check   = 0;
    int     new_sock                = 0;
    int     max_fd                  = 0;
    int     fd                      = 0;
    int     i                       = 0;
    int     err                     = RDLR_TCP_CLOSED;
    int     ret                     = 0;

    q_fd_set sockSet;
    struct sockaddr_in  fromAddr;
    A_UINT8 *recvBuff = qcom_mem_alloc(RDLR_CREDIT_SIZE);

    struct timeval tmo;
    memset (&tcp_cli, 0, 4);

    while (1) {

        if (g_connect_state == 0) {
            rdlr_credential_check = 0;
                break;
        }
            /* Wait for Input */
        memset (&fromAddr, 0, sizeof (struct sockaddr_in));
        fromSize = sizeof (struct sockaddr_in);

        FD_ZERO(&sockSet);
        FD_SET(tcp_socket, &sockSet);
        max_fd = tcp_socket;

        tmo.tv_sec = 5;
        tmo.tv_usec= 0;

        for (i = 0; i < MAX_TCP_CLI; i++) {
            fd = tcp_cli[i];
            if (fd > 0) {
            FD_SET(fd, &sockSet);
            }
            if (fd > max_fd) {
            max_fd = fd;
            }
        }

        fdAct = qcom_select (max_fd + 1, &sockSet, NULL, NULL, &tmo);

        if (fdAct < 0) {
            err = RDLR_SOCK_LS_ERROR;
            break;
        } else if (fdAct == 0) {
            continue;
        }

        fdAct = FD_ISSET (tcp_socket, &sockSet);

        if (fdAct == 1) {

            fromSize = sizeof (struct sockaddr_in);
            new_sock = qcom_accept (tcp_socket, (struct sockaddr *) &fromAddr, &fromSize);
            if (new_sock < 0) {
                continue;
            }

            for (i = 0; i < MAX_TCP_CLI; i++) {
                if (tcp_cli[i] == 0) {
                    tcp_cli[i] = new_sock;
                    break;
                }
            }

        }

        for (i = 0; i < MAX_TCP_CLI; i++) {

             fd = tcp_cli[i];

             fdAct = FD_ISSET(fd, &sockSet);

             if (fdAct) {

                memset(recvBuff, 0, RDLR_CREDIT_SIZE);
                recvBytes = qcom_recv(fd, (char *)recvBuff, RDLR_CREDIT_SIZE, 0);

                if (recvBytes <= 0) {
                    qcom_socket_close(fd);
                    tcp_cli[i] = 0;
                } else {
                    switch_uart_mode (0);   // Going into Data Mode
                    ret = ticket_process(fd, recvBuff, recvBytes);
                    if (ret < 0) {
                        qcom_socket_close(fd);
                        tcp_cli[i] = 0;
                    }
                    qcom_thread_msleep(5);
                }
            }
        }

        if (g_connect_state == 0) {
            rdlr_credential_check = 0;
            break;
        }

        if (!tcp_socket) {
            rdlr_credential_check = 0;
            break;
        }

        if (rdlr_credential_check) {
            break;
        }
    }

    qcom_mem_free (recvBuff);
    recvBuff = NULL;

    if (tcp_socket) {
        qcom_socket_close(tcp_socket);
        memset(&tcp_cli, 0, 4);
        tcp_socket = 0;
    }

    qcom_thread_msleep(100);
    qcom_task_exit();
}

void process_RDLR_ServerInit (int port) {

    A_INT32 ret     = 0;
    A_UINT32 isFirst    = 0;

    struct sockaddr_in sock_addr;
    memset ((char*)&sock_addr, 0, sizeof (struct sockaddr_in));

    if ( tcp_socket ) {
        AT_PTF("ERROR-03.1.1");
        return;
    }

    tcp_socket = qcom_socket (AF_INET, SOCK_STREAM, 0);

    if (tcp_socket < 0) {
        AT_PTF("ERROR-03.1.1");
        return;
    }

    // Get the parameters here. Port number
    sock_addr.sin_port   = htons (port);
    sock_addr.sin_family = AF_INET;

    ret = qcom_bind (tcp_socket, (struct sockaddr *) &sock_addr, sizeof (struct sockaddr_in));

    if (ret < 0) {
        AT_PTF("ERROR-03.1.1");
        return ;
    }

    if (0 == isFirst) {
        ret = qcom_listen (tcp_socket, MAX_TCP_CLI);
        if (ret < 0) {
            /* Close Socket */
            AT_PTF("ERROR-03.1.1");
        }
        isFirst = 1;
    }

    qcom_task_start (at_tcp_server_receive, 0, 2048, 50);
}

void process_RDLR_SetSrvr(char* cmd_str, A_UINT16 cmd_len) {

    int port = 0;
    do {

        if (g_connect_state == 0) {
            AT_PTF("ERROR-03.1.1");
            return;
        }

        if (tcp_socket) {
            AT_PTF("ERROR-03.1.1");
            break;
        }

        qcom_power_set_mode (0, MAX_PERF_POWER);
        // Get the parameters here. Port number
        A_SSCANF (cmd_str, "%d", &port);

        if ((port < 1024 )|| (port > 65534))
        {
            AT_PTF("ERROR-03.1.1");
            break;
        }

        process_RDLR_ServerInit (port);

    } while(0);
}

void process_RDLR_SocketClose (char* cmd_str, A_UINT16 cmd_len) {


    if (tcp_cli_socket) {
    if (ssl) {
            if (ssl->ssl) {
                qcom_SSL_shutdown(ssl->ssl);
            ssl->ssl = NULL;
        }
        if (ssl->sslCtx) {
                qcom_SSL_ctx_free(ssl->sslCtx);
            ssl->sslCtx = NULL;
        }
        }
        qcom_socket_close (tcp_cli_socket);
    tcp_cli_socket = 0;
    }

    if (tcp_socket) {
        qcom_socket_close(tcp_socket);
    tcp_socket     = 0;
        memset(&tcp_cli, 0, 4);
    }

//    AT_PTF ("R=%d--!", RDLR_OK);
}

void process_HELP (char* cmd_str, A_UINT16 cmd_len)
{
    AT_PTF (RDLR_HELP);
}

void wifi_conn_callback (A_UINT8 device_id, int value) {

   // qcom_thread_msleep (5000);

 //  AT_PTF("device_id = %d and value = %d\n", device_id, value);

}

int qca_md5_calc (unsigned char * d, unsigned char * msg, A_INT32 msg_len)
{
    if ((d == NULL) || (msg == NULL)) {
        return -1;
    }
    qcom_sec_md5_init();
    qcom_sec_md5_update(msg, msg_len);
    qcom_sec_md5_final((char *)d);
    return 0;
}

void process_RDLR_PSUS(char* cmd_str,A_UINT16 cmd_len){

    A_UINT32  sleep_time;
    A_UINT32  flag;
    A_UINT32  gpio31_config;
    A_UINT32  gpio30_config;

//    AT_PTF("command = %s\n", cmd_str);
    A_SSCANF(cmd_str, "%d,%d,%d", &sleep_time, &gpio31_config, &gpio30_config);
    qcom_wlan_suspend_enable();

    if (sleep_time) {
        flag = WAKE_MGR_WAKE_EVENT_TIMER;
    } else {
        flag = 0;
    }
    switch(gpio31_config) {
    case    1:
        flag |= WAKE_MGR_WAKE_EVENT_GPIO31_HIGH;
        break;
    case    2:
        flag |= WAKE_MGR_WAKE_EVENT_GPIO31_LOW;
        break;
    }

    switch(gpio30_config) {
    case    1:
        flag |= WAKE_MGR_WAKE_EVENT_GPIO30_HIGH;
        break;
    case    2:
        flag |= WAKE_MGR_WAKE_EVENT_GPIO30_LOW;
        break;
    }

//    AT_PTF("flag: 0x%x\n", flag);
    if (flag == 0) {
//        AT_PTF("ERROR: Please enter a valid wakeup options\n");
        return;
    }
    qcom_wm_suspend(sleep_time, flag);

}


void swat_wmiconfig_sta_info(A_UINT8 device_id)
{
  A_UINT8 num = 0;
  A_UINT8 mac[60];
  A_MEMSET(mac, 0, 60);
  qcom_ap_get_sta_info(device_id, &num, mac);
  if (num == 0)
  {
//    AT_PTF("there are no stations connected\n");
  }
  else
  {
//    AT_PTF("there are stations connected :\n");
  }
}


void process_RDLR_METADATA (char* cmd_str, A_UINT16 cmd_len) {

    A_UINT32    wlan_ver = RDLR_WLAN_VERSION;
    char        verStr[64];
    char        apmacStr[20];
    A_UINT8     apmacAddr[6];
    A_UINT8     mac[6];
    char        sta_mac[50];
    char        battery_life[20];

    A_MEMSET(metadata_buffer, 0, sizeof(metadata_buffer));
    A_MEMSET(apmacAddr, 0, sizeof(apmacAddr));
    A_MEMSET(verStr, 0, sizeof(verStr));
    A_MEMSET(sta_mac, 0, sizeof(sta_mac));

    // Getting Version
    qcom_sprintf(verStr, "%d.%d.%d.%d.%d",
                     (wlan_ver&0xF0000000)>>28,
                     (wlan_ver&0x0F000000)>>24,
                     (wlan_ver&0x00FC0000)>>18,
                     (wlan_ver&0x0003FF00)>>8,
                     (wlan_ver&0x000000FF));

    // Getting AP-MAC id.
    qcom_mac_get(currentDeviceId, (A_UINT8 *) &apmacAddr);
    qcom_sprintf(apmacStr, "%02X:%02X:%02X:%02X:%02X:%02X",
            apmacAddr[0], apmacAddr[1], apmacAddr[2],
            apmacAddr[3], apmacAddr[4], apmacAddr[5]);

    // Getting STA-MAC id.
    if (station_count != 0) {
        memcpy(mac, station_mac_arr[0], 6);
            qcom_sprintf(sta_mac,"%X:%X:%X:%X:%X:%X,",
                mac[0], mac[1],mac[2],mac[3],mac[4],mac[5]);
    }

    //Getting Battery Percentage
    rdlr_get_battery_percentage (battery_life);
    qcom_sprintf(metadata_buffer, "\"%s,%s,%s%s,OK\"",  verStr, apmacStr, sta_mac, battery_life);

    AT_PTF(metadata_buffer);
}

A_INT8 Rdlr_DataStrCpy(A_UINT8 *pDest, A_UINT8 *pSrc, int8_t maxLen) {

    A_UINT8 *pTempD = pDest;
    A_UINT8 *pTempS = pSrc;
    int8_t len;

    if (*pTempS != '\"') {
        return -1;
    }

    pTempS++;

    for (len = 0; len < maxLen; len++) {
        if (*pTempS == '\"') {
            *pTempD = '\0';
            break;
        } else {
            *pTempD++ = *pTempS++;
        }
    }

    if (len == maxLen) {
        return -1;
    }

    return len;
}

A_UINT32 DhcpCallback(A_UINT8 * mac, A_UINT32 addr)
{

    memcpy(station_mac_arr[0], mac, 6);
    station_ip_addr[0] = addr;
    station_count = 1;

    return RDLR_OK;
}

void process_RDLR_MAC (char* cmd_str, A_UINT16 cmd_len) {

    int i = 0;
    A_UINT32 addr = 0;
    A_UINT8  *mac = 0;

    AT_PTF("total client = %d\n", station_count);
    for (i = 0; i < station_count; i++) {
        mac = station_mac_arr[i];
        addr = station_ip_addr[i];
        AT_PTF("MAC of station %d is :%x:%x:%x:%x:%x:%x IP:%d.%d.%d.%d\n",
                            i, mac[0],mac[1],mac[2],mac[3],mac[4],mac[5],
                            (addr >>24) & 0xff, (addr>>16) & 0xff, (addr >>8) & 0xff, addr & 0xff);
    }
    AT_PTF("Done\n");
}

void process_RDLR_SETTRIP (char* cmd_str, A_UINT16 cmd_len) {

    QCOM_WLAN_DEV_MODE mode;
    A_UINT8     ssid[32];
    A_INT8      ssidlen = 0;

    A_MEMSET(&ssid,    0, sizeof (ssid));

    // AP Mode Starting Process
    qcom_op_get_mode(currentDeviceId, &mode);

    if (mode == QCOM_WLAN_DEV_MODE_AP) {
        AT_PTF (RDLR_OK_STR);
        return;
    }

    if (tcp_socket) {
        qcom_socket_close(tcp_socket);
        memset(&tcp_cli, 0, 4);
        tcp_socket = 0;
    }

    // Disconnect
//    process_WD (NULL, 0);

    ssidlen = Rdlr_DataStrCpy(ssid, (A_UINT8 *)cmd_str, 32);
    if (ssidlen < 0) {
        AT_PTF("ERROR-03.1");
        return;
    }


    qcom_op_set_mode(currentDeviceId, QCOM_WLAN_DEV_MODE_AP);
    qcom_set_connect_callback(currentDeviceId, wifi_conn_callback);

    apState = AP_INIT_OK;

    qcom_power_set_mode (currentDeviceId, MAX_PERF_POWER);
    qcom_sec_set_auth_mode(currentDeviceId, WLAN_AUTH_WPA2_PSK);
    qcom_sec_set_passphrase(currentDeviceId, RDLR_PASSWORD);
    qcom_sec_set_encrypt_mode(currentDeviceId, WLAN_CRYPT_AES_CRYPT);
    qcom_set_channel(currentDeviceId, RDLR_AP_CHANNEL);

//  qcom_ap_start (currentDeviceId, (char *)ssid);

    qcom_set_ssid(currentDeviceId,(char *)ssid);
    qcom_commit(currentDeviceId);

    qcom_thread_msleep (500);

    qcom_ip_address_set (currentDeviceId, RDLR_IPA, RDLR_MASK, RDLR_GW);
    qcom_thread_msleep (500);
    qcom_dhcps_set_pool(currentDeviceId, RDLR_START_IP, RDLR_END_IP, RDLR_IP_LEASE);
    qcom_thread_msleep (500);

    memset(station_mac_arr, 0, sizeof(station_mac_arr));
    memset(station_ip_addr, 0, sizeof(station_ip_addr));
    station_count=0;

    qcom_dhcps_register_cb (currentDeviceId, DhcpCallback);
    qcom_thread_msleep (500);

    g_connect_state = 1;

    // Starting Server
    process_RDLR_SetSrvr(RDLR_SERVER_PORT, 4);
    AT_PTF (RDLR_OK_STR);
}

void process_RDLR_GETDATA (char* cmd_str, A_UINT16 cmd_len) {

    int         i = 0;
    int         ticket_num = 0;
    int         tickets_count = 0;
    int         entry = 0;
    T_PARAM     *param = &t_param;
    T_INFO      *param_i = &t_info;
    int         ret = 0;
    A_UINT32    offset = 0;
    A_UINT32    index = 0;


    memset(param, 0, sizeof(T_PARAM));
    memset(param_i, 0, sizeof(T_INFO));
    ret = tickets_param_load_flash(param);
    if (ret == -1) {
        AT_PTF("\"#,OK\"\n");
        return;
    }

    tickets_count = param->issued_tickets_count;
//  AT_PTF("tickets_count  = %d\n", tickets_count);
    sscanf(cmd_str, "%d", &ticket_num);

    if ((ticket_num >= tickets_count) && (ticket_num < 0)) {
        AT_PTF("ERROR-06");
        return;
    }

    offset = ticket_num * sizeof(T_INFO);
    strcpy(ticket_raw, "\"");
    index++;
    for (i = ticket_num; i < tickets_count; i++) {
        memset(param_i, 0, sizeof(T_INFO));
        tickets_info_load_flash(param_i, offset);
        if (!entry) {
            entry = 1;
            strcpy(ticket_raw + index, "#,");
            index += 2;
            AT_PTF("param -> %s\n", param_i->ticket_raw);
            memcpy(ticket_raw + index, param_i->ticket_raw, strlen(param_i->ticket_raw) + 1);
        } else {
            memset(ticket_raw, 0, sizeof(ticket_raw));
            strcpy(ticket_raw, "#,");
            index += 2;
            AT_PTF("param -> %s\n", param_i->ticket_raw);
            memcpy(ticket_raw + index, param_i->ticket_raw, strlen(param_i->ticket_raw) + 1);
        }

        strcpy(ticket_raw + strlen(ticket_raw), ",");
        AT_PTF(ticket_raw);
        memset(ticket_raw, 0, sizeof(ticket_raw));
        qcom_thread_msleep (20);
        index = 0;
        offset += sizeof(T_INFO);
    }
    if (ticket_num == tickets_count) {
        AT_PTF("\"");
    }
    AT_PTF("#,OK\"\n");
}

void process_RDLR_CHECK (char* cmd_str, A_UINT16 cmd_len) {

    A_CHAR buf[20];
    T_PARAM *param = &t_param;
    int ret = 0;
    char battery_life[20];

    // RES - Battery percentage
    rdlr_get_battery_percentage (battery_life);
    qcom_sprintf(buf, "\"%s", battery_life);
    qcom_sprintf(buf, "%s,", buf);

    // RES - No-Of-Mtickets
    memset(param, 0, sizeof(*param));
    ret = tickets_param_load_flash(param);
    if (ret == 1) {
        qcom_sprintf(buf, "%s%d", buf, param->issued_tickets_count);
    }
    qcom_sprintf(buf, "%s,", buf);

    // RES - No-of-Passes


    qcom_sprintf(buf, "%s,%s", buf, RDLR_OK_STR);
    AT_PTF(buf);
}

void process_RDLR_VALIDATE (char* cmd_str, A_UINT16 cmd_len) {

//    A_CHAR    buffer[256];
    A_UINT32    ticket_code = 0;

    sscanf(cmd_str, "%d", &ticket_code);

    AT_PTF(RDLR_OK_STR);
    // Pending.......depends on ticket code
}

void process_RDLR_SETSTAGE (char* cmd_str, A_UINT16 cmd_len) {

//    A_CHAR    buffer[3];
    A_UINT32    stage_no = 0;

    sscanf(cmd_str, "%d", &stage_no);

    AT_PTF(RDLR_OK_STR);
    // Pending.......depends on stage number

}

void process_RDLR_SHUTDOWN (char* cmd_str, A_UINT16 cmd_len) {

    QCOM_WLAN_DEV_MODE mode;
    T_PARAM *param = &t_param;
    T_INFO  *param_i = &t_info;
    int ret = 0;

    memset(param, 0, sizeof(T_PARAM));
    memset(param_i, 0, sizeof(T_INFO));

    qcom_thread_msleep (300);
    // Stop the server
    process_RDLR_SocketClose (NULL, 0);

    /* Erasing a Flash Memory */
    ret = tickets_param_load_flash(param);
    if (ret == 1) {
        flash_erase(QBC_TICKET_INFO_ADDR, param->ticket_offset);
        flash_erase(QBC_TICKET_PARAM_ADDR, sizeof(T_PARAM));
    }

    // Moving to Station Mode
    qcom_op_get_mode (currentDeviceId, &mode);

    if (mode != QCOM_WLAN_DEV_MODE_AP) {
        goto ret;
    }

    qcom_op_set_mode(currentDeviceId, QCOM_WLAN_DEV_MODE_STATION);
    qcom_thread_msleep (30);

ret:
    qcom_thread_msleep (100);
    /* WiFi Module Going into sleep mode */
    qcom_gpio_pin_dir(1, 0);
    qcom_gpio_pin_set(1, 0);

//    AT_PTF(RDLR_OK_STR); //After Moving into sleep we cant send any data.

}

void process_RDLR_GPIO (char* cmd_str, A_UINT16 cmd_len) {

    int pin = 0;
    char input[10];
    A_UINT16  token_len = 0;
    char      token_buf[64];

    //get the flag
    A_MEMSET (token_buf, 0, 64);
    token_len = (cmd_len<64) ? cmd_len : 64; //maximum len for this token
    copy_data_upto (cmd_str, token_buf, &cmd_len, &token_len, 1, ',');
    sscanf (token_buf, "%d", &pin);

    token_len++; //skip ','
    cmd_str += token_len;
    cmd_len -= token_len;


     //get the port number
    A_MEMSET (token_buf, 0, 64);
    token_len = (cmd_len<64) ? cmd_len : 64; //maximum len for this token
    copy_data_upto (cmd_str, token_buf, &cmd_len, &token_len, 1, ',');
    strcpy(input, token_buf);
//    AT_PTF("GPIO Pin = %d and params = %s\n", pin, input);

    qcom_gpio_pin_dir(pin, 0);

    if (!strcmp(input, "on")) {
        qcom_gpio_pin_set(pin, 1);
    } else {
        qcom_gpio_pin_set(pin, 0);
    }
}

void rdlr_get_battery_percentage (char *battery_life)
{

    ADC_CHAN_CFG_T adc_channel[1];
    int ret = -1;
    int i = 0;
    uint32_t len = 0;
    uint8_t more = 0;
    uint8_t done = 0;
    uint8_t adc_buf[16];
    uint32_t sum = 0;
    uint32_t val = 0;
    double  per_value = 0.0;

#define ADC_MAX_V   3.000
#define ADC_MIN_V   2.000

    ret = qcom_adc_init((A_UINT8)GAIN_SCALE_3V3, 12/* accuracy */, ADC_FREQ_31P25_KHZ, (A_UINT8)SINGLE_ENDED, 0);

    if (ret < 0) {
//        AT_PTF("ADC init failed!\n");
        return;
    } else {
//        AT_PTF("ADC init done!\n");
    }

    adc_channel[0].adch = 0;
    adc_channel[0].input_type = 0;

    ret = qcom_adc_config(0 /*single channel*/, 0 /*software trigger*/, 1 /*dynamic scan/sample-by-sample*/, adc_channel, 1 /*channel count*/);
    if (ret == 0) {
//        AT_PTF("config done\n");
    } else {
//        AT_PTF("comnfig not done\n");
    }

    qcom_adc_dma_config(16, 8);       /* get 8 samples */

    qcom_thread_msleep (100);
    ret = qcom_adc_conversion(1);
    if (ret != 0) {
//        AT_PTF("conversion not started\n");
    } else {
//        AT_PTF("conversion started\n");
    }
    qcom_thread_msleep (1000);

    A_MEMSET(adc_buf, 0, sizeof(adc_buf));
    qcom_adc_recv_data(0 /*channel no*/, adc_buf, 16, &len, &more, &done);

//    AT_PTF("len = %d more = %d done = %d\n", len, more, done);
//    if (done) {
//        AT_PTF("yes......\n");
//    }

//    AT_PTF("\n");
//    for (i = 0; i < len; i++) {
//        AT_PTF("%x ", adc_buf[i]);
//    }
//    AT_PTF("\n");

//    AT_PTF("adc0 data: ");

    for(i=0, sum=0;i<8;i++)
    {
//        AT_PTF("%x%x ", adc_buf[2*i+1], adc_buf[2*i]);
        val = adc_buf[i * 2 + 1];
        val <<= 8;
        val |= adc_buf[i * 2];
        val >>= 4;
        sum += val;
    }
//    AT_PTF("result = 0x%x (%u)\n",sum>>2 , sum>>2);
//    AT_PTF("result = 0x%x (%u)\n",(sum>>2)/8 , (sum>>2)/8);
    sum >>= 2;      //  12 bit accuracy actually will give 10 bit accuracy
    sum /= 8;       //  Average of total
    //AT_PTF("main result = %d\n", sum);

    per_value = (((sum * (3.3 * 1.35)) / 1023) - ADC_MIN_V);
    if (per_value < 0) {
        per_value = 0;
    } else {
        per_value = (per_value * 100) / (ADC_MAX_V - ADC_MIN_V);
    }

    if (per_value >= 100.0) {
        per_value = 100.000000;
    }

//    AT_PTF("Battery Percentage = %f%%\n", per_value);
    ftoa(per_value, battery_life);
//  AT_PTF("battery_life = %s\n", battery_life);

//    AT_PTF("\n");
    qcom_adc_close();

}

void process_RDLR_ADC (char* cmd_str, A_UINT16 cmd_len) {

    char battery_life[20];

    rdlr_get_battery_percentage(battery_life);
//  AT_PTF("main_battery_life = %s\n", battery_life);

}

void process_RDLR_OTA(char* cmd_str, A_UINT16 cmd_len) {

    A_UINT16  token_len = 0;
    char      token_buf[64];
    uint32_t  port = 0;
    char      server_ip[64];
    char      file[32];
    T_PARAM *param = &t_param;
    T_INFO  *param_i = &t_info;
    int ret = 0;

    memset(param, 0, sizeof(T_PARAM));
    memset(param_i, 0, sizeof(T_INFO));

    //get the port
    A_MEMSET (token_buf, 0, 64);
    token_len = (cmd_len<64) ? cmd_len : 64; //maximum len for this token
    copy_data_upto (cmd_str, token_buf, &cmd_len, &token_len, 1, ',');
    sscanf (token_buf, "%d", &port);

    token_len++; //skip ','
    cmd_str += token_len;
    cmd_len -= token_len;

     //get the ip
    A_MEMSET (token_buf, 0, 64);
    token_len = (cmd_len<64) ? cmd_len : 64; //maximum len for this token
    copy_data_upto (cmd_str, token_buf, &cmd_len, &token_len, 1, ',');
    strcpy(server_ip, token_buf);

    token_len++; //skip ','
    cmd_str += token_len;
    cmd_len -= token_len;

     //get the file
    A_MEMSET (token_buf, 0, 64);
    token_len = (cmd_len<64) ? cmd_len : 64; //maximum len for this token
    copy_data_upto (cmd_str, token_buf, &cmd_len, &token_len, 1, ',');
    strcpy(file, token_buf);

    token_len++; //skip ','
    cmd_str += token_len;
    cmd_len -= token_len;


    /* NOTE - Before starting OTA Needs to Erasing a Flash Memory */
    ret = tickets_param_load_flash(param);
    if (ret == 1) {
        flash_erase(QBC_TICKET_INFO_ADDR, param->ticket_offset);
        flash_erase(QBC_TICKET_PARAM_ADDR, sizeof(T_PARAM));
    }

    rdlr_ota_https_upgrade(port, server_ip, file);
    qcom_thread_msleep (300);
    qcom_sys_reset();

}

void process_RDLR_Erase(char* cmd_str, A_UINT16 cmd_len) {

    unsigned int addr = 0, size = 0;
    int ret = -1;
    /* This Function can Erase only in multiple of Sector size */
    sscanf(cmd_str, "0x%x,%d", &addr, &size);
//    AT_PTF("addr = 0x%x size = %d\n", addr, size);
    ret = qcom_nvram_erase(addr, size);
//    AT_PTF("ret = %d\n", ret);
}

void station_conn_callback (A_UINT8 device_id, int value) {

    A_UINT32 reason;
    A_UINT32 devmode;

    qcom_op_get_mode(currentDeviceId, &devmode);

    if (value == 10 && devmode == 0) {
        value = 0; // Invalid Profile
    }

    if (!value) {
    qcom_get_disconnect_reason (currentDeviceId, &reason);
//    AT_PTF ("R=%d,%d--!",BSL_DISCONNECTED, reason);
    }

    if (value) {
        g_connect_state = 1;
    if (devmode == 1) {
        if (apState == AP_INIT_OK) {
        apState = AP_STATE_UP;
//        AT_PTF ("R=%d--!", BSL_AP_UP);
        } else {
        apState = AP_STA_OK;
//        AT_PTF ("R=%d--!", BSL_AP_CLI_CONNECT);
        }
    }
    } else {
        if ((devmode == 1 && reason != 4) || (devmode != 1)) {
            g_connect_state = 0;
            qcom_thread_msleep (5000);
        }
    }

}


void rdlr_process_Connect (A_UINT8 mode, char * ssid, char * password, A_INT8 auth, A_INT8 encr) {

    int ret     = 0;
    uint32_t  ip = 0, mask = 0, gateway = 0;
    int32_t  timeout = rdlr_timeout;
    A_INT8 error_t = 0;
    A_UINT32 g_mode = 0;

    rdlr_init_progress = 1;
    g_connect_state   = 0;
//  AT_PTF("mode = %d ssid = %s password = %s auth = %d encr = %d\n", mode, ssid, password, auth, encr);

    do {

        if (mode == 1 /* station */) {
            process_WD (NULL, 0);
            qcom_op_set_mode(currentDeviceId, QCOM_WLAN_DEV_MODE_STATION);

            if (auth == 0x00 && encr == 0x00) { // OPEN
                qcom_sec_set_auth_mode(currentDeviceId, WLAN_AUTH_NONE);
            }

            if (auth == 0x01 || encr == 0x01) { // WEP
                qcom_sec_set_auth_mode(currentDeviceId, WLAN_AUTH_NONE);
                qcom_sec_set_wepkey (currentDeviceId, 1, password);
                qcom_sec_set_wepkey_index (currentDeviceId, 1);
                qcom_sec_set_wep_mode (currentDeviceId, 2);
            }

            if (auth == 0x02) { // WPA
                qcom_sec_set_auth_mode(currentDeviceId, WLAN_AUTH_WPA_PSK); //WLAN_AUTH_MODE
                if (encr == 0x02) {
                    qcom_sec_set_encrypt_mode(currentDeviceId, WLAN_CRYPT_TKIP_CRYPT);
                } else {
                    qcom_sec_set_encrypt_mode(currentDeviceId, WLAN_CRYPT_AES_CRYPT);
                }
                qcom_sec_set_passphrase(currentDeviceId, password);

            } else if (auth >= 0x03) { // WPA2
                qcom_sec_set_auth_mode(currentDeviceId, WLAN_AUTH_WPA2_PSK); //WLAN_AUTH_MODE
                if (encr == 0x02) {
                    qcom_sec_set_encrypt_mode(currentDeviceId, WLAN_CRYPT_TKIP_CRYPT);
                } else {
                    qcom_sec_set_encrypt_mode(currentDeviceId, WLAN_CRYPT_AES_CRYPT);
                }
                qcom_sec_set_passphrase(currentDeviceId, password);
            }

            qcom_set_connect_callback(currentDeviceId, station_conn_callback);

            do {

                qcom_sta_connect_with_scan(currentDeviceId, ssid);

                if (g_connect_state == 1) {
                    break;
                }

                qcom_thread_msleep (WIFI_CONNECT_SLEEP);
                timeout -= WIFI_CONNECT_SLEEP;

            } while ((g_connect_state != 1) && (timeout > 0));

            if (g_connect_state == 1) {
                qcom_dhcpc_enable (currentDeviceId, 1);  // turn on dhcp client
                dhcpc_check = 1;
            } else {
                error_t = 1;
                goto error;
            }

            qcom_ip_address_set (currentDeviceId, 0, 0, 0);

            qcom_ipconfig (currentDeviceId, IP_CONFIG_DHCP, &ip, &mask, &gateway);

            timeout = 10000;
            while (0 == ip) {
                if (timeout < 0) {
                    error_t = 1;
                    goto error;
                }

                qcom_thread_msleep (DHCP_WAIT);
                if (A_OK != qcom_ip_address_get (currentDeviceId, &ip, &mask, &gateway)) {
                    error_t = 1;
                    goto error;
                }

                timeout -= DHCP_WAIT;
            }

            qcom_dnsc_enable (1);
            gdns_cli_enable = 1;

            ip  = 0;
            ret = 0;

            ret = qcom_ping (pinginetIp, DEFAULT_PING_SIZE);

            if (ret == A_OK) {
                inetEnabled = 1;
                AT_PTF (RDLR_OK_STR);
            } else {
                inetEnabled = 0;
                error_t = 1;
                goto error;
            }
        } else if (mode == 2 /* ap */) {
            qcom_op_get_mode(currentDeviceId, &g_mode);

            if (g_mode == QCOM_WLAN_DEV_MODE_AP) {
                AT_PTF(RDLR_OK_STR);
                return;
            }

            if (tcp_socket) {
                qcom_socket_close(tcp_socket);
                memset(&tcp_cli, 0, 4);
                tcp_socket = 0;
            }

            qcom_power_set_mode (currentDeviceId, MAX_PERF_POWER);

            qcom_op_set_mode(currentDeviceId, QCOM_WLAN_DEV_MODE_AP);
            qcom_set_connect_callback(currentDeviceId, wifi_conn_callback);

            apState = AP_INIT_OK;

            qcom_sec_set_auth_mode(currentDeviceId, WLAN_AUTH_WPA2_PSK);
            qcom_sec_set_passphrase(currentDeviceId, password);
            qcom_sec_set_encrypt_mode(currentDeviceId, WLAN_CRYPT_AES_CRYPT);
            qcom_set_channel(currentDeviceId, RDLR_AP_CHANNEL);

//          qcom_ap_start (currentDeviceId, (char *)ssid);

            qcom_set_ssid(currentDeviceId,(char *)ssid);
            qcom_commit(currentDeviceId);

            qcom_thread_msleep (2000);

            qcom_ip_address_set (currentDeviceId, RDLR_IPA, RDLR_MASK, RDLR_GW);
            qcom_thread_msleep (2000);
            qcom_dhcps_set_pool(currentDeviceId, RDLR_START_IP, RDLR_END_IP, RDLR_IP_LEASE);
            qcom_thread_msleep (2000);
            qcom_dhcps_register_cb (currentDeviceId, DhcpCallback);


            qcom_thread_msleep (1000);
            g_connect_state = 1;
            AT_PTF (RDLR_OK_STR);
        }
    } while (0);

error:
    if (error_t == 1) {
        AT_PTF("ERROR-12.3");
    } else if (error_t == 2) {
        AT_PTF("ERROR-12.2");
    }

    rdlr_init_progress = 0;
    qcom_thread_msleep (100);
}

void process_RDLR_CWMODE(char* cmd_str, A_UINT16 cmd_len) {

    A_UINT16  token_len = 0;
    char      token_buf[64];
    A_INT32 mode = 0;
    A_INT32 auth = 0;
    A_INT32 encry = 0;
    char ssid[32];
    char password[32];

    //get the mode
    A_MEMSET (token_buf, 0, 64);
    token_len = (cmd_len<64) ? cmd_len : 64; //maximum len for this token
    copy_data_upto (cmd_str, token_buf, &cmd_len, &token_len, 1, ',');
    sscanf (token_buf, "%d", &mode);

    token_len++; //skip ','
    cmd_str += token_len;
    cmd_len -= token_len;

    //get the ssid
    A_MEMSET (token_buf, 0, 64);
    token_len = (cmd_len<64) ? cmd_len : 64; //maximum len for this token
    copy_data_upto (cmd_str, token_buf, &cmd_len, &token_len, 1, ',');
    strcpy(ssid, token_buf);

    token_len++; //skip ','
    cmd_str += token_len;
    cmd_len -= token_len;

    //get the auth
    A_MEMSET (token_buf, 0, 64);
    token_len = (cmd_len<64) ? cmd_len : 64; //maximum len for this token
    copy_data_upto (cmd_str, token_buf, &cmd_len, &token_len, 1, ',');
    sscanf (token_buf, "%d", &auth);

    token_len++; //skip ','
    cmd_str += token_len;
    cmd_len -= token_len;

    //get the encryption
    A_MEMSET (token_buf, 0, 64);
    token_len = (cmd_len<64) ? cmd_len : 64; //maximum len for this token
    copy_data_upto (cmd_str, token_buf, &cmd_len, &token_len, 1, ',');
    sscanf (token_buf, "%d", &encry);

    token_len++; //skip ','
    cmd_str += token_len;
    cmd_len -= token_len;

    //get the password
    A_MEMSET (token_buf, 0, 64);
    token_len = (cmd_len<64) ? cmd_len : 64; //maximum len for this token
    copy_data_upto (cmd_str, token_buf, &cmd_len, &token_len, 1, ',');
    strcpy(password, token_buf);

    token_len++; //skip ','
    cmd_str += token_len;
    cmd_len -= token_len;

    if (strlen(ssid) > 32 || strlen(password) > 32 || auth > 3 || encry > 3) {
        AT_PTF("ERROR-12.1");
        return;
    }
    rdlr_process_Connect(mode, ssid, password, auth, encry);

}

void process_RDLR_GIP(char* cmd_str, A_UINT16 cmd_len)
{
    A_UINT32 ip;
    A_UINT32 submask;
    A_UINT32 gateway;
    A_CHAR   ipStr[64];

    A_MEMSET(&ipStr, 0, sizeof (ipStr));
    qcom_ipconfig(currentDeviceId, IP_CONFIG_QUERY, &ip, &submask, &gateway);

    qcom_sprintf (ipStr, "%d.%d.%d.%d", (ip>>24&0xFF), (ip>>16&0xFF), (ip>>8&0xFF), (ip&0xFF));

    AT_PTF("ip = %s\n", ipStr);
}

